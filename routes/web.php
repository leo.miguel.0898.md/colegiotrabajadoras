<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

/* WEB */

Route::get('/', 'Web\WebController@index')->name('/');
Route::get('institucional', 'Web\WebController@institucional')->name('institucional');
Route::get('historia', 'Web\WebController@historia')->name('historia');
Route::get('mision', 'Web\WebController@mision')->name('mision');
Route::get('consejos', 'Web\WebController@consejos')->name('consejos');
Route::get('requisitos', 'Web\WebController@requisitos')->name('requisitos');
Route::get('contacto', 'Web\WebController@contacto')->name('contacto');
Route::get('comunicado', 'Web\WebController@comunicados')->name('comunicados');
Route::get('detallecomunicado/{id}', 'Web\WebController@detallecomunicado')->name('detallecomunicado');
Route::post('contactos', 'EmailController@store')->name('contacto-mensaje');
Route::get('evento', 'Web\WebController@eventos')->name('eventos');
Route::get('detalleevento/{id}', 'Web\WebController@detalleevento')->name('detalleevento');
Route::get('tramite', 'Web\WebController@tramites')->name('tramites');
Route::get('detalletramite/{id}', 'Web\WebController@detalletramites')->name('detalletramite');
/* SISTEMA */
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('sliders', 'Admin\SlidersController');
Route::resource('eventos', 'Admin\EventosController');
Route::resource('clientes', 'Admin\ClientesController');
Route::post('importarExcel', 'Admin\ClientesController@importarExcel')->name('importarExcel');
Route::resource('comunicados', 'Admin\ComunicadosController');
Route::resource('tramites', 'Admin\TramitesController');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::resource('excel', 'ExcelController');

Route::get('nombres', 'Web\WebController@buscador')->name('paginas');
Route::get('nombres/buscador', 'Web\WebController@buscador')->name('buscador');
