@if(session('flash'))
                <div class="alert alert-success" role="alert">
                    <strong>Aviso</strong>&nbsp&nbsp{{session('flash')}}
                    <button type="button" class="close" data-dismiss="alert" alert-label="close"><span area-hidden="true">&times;</span></button>
                </div>
                @endif
