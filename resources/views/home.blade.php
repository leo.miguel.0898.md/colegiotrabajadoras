@extends('admin.layouts.layout-admin')
@section('titulo')
    @include('admin.layouts.titulo-admin')
@endsection


@section('menu_items') <!-- Administrador -->
    @include('admin.pages.menu')
@endsection

@section('content')
    @include('admin.layouts.content_index')
@endsection