@extends('web.layouts.layout')
@section('style')
<style type="text/css">
    .slider {
    width: 100%;
    margin: auto;
    overflow: hidden;
}

.slider ul {
    display: flex;
    padding: 0;
    width: 300%;

    animation: cambio 20s infinite;
    animation-direction:alternate linear;
}

.slider li {
    width: 100%;
    list-style: none;
}

.slider img {
    width: 100%;
}

@keyframes cambio {
    0% {margin-left: 0;}
    20% {margin-left: 0;}

    25% {margin-left: -100%;}
    45% {margin-left: -100%;}

    50% {margin-left: -200%;}
    70% {margin-left: -200%;}
}

</style>
@endsection
@section('content')
    <div class="slider" >
            <ul style="margin-top: 0!important; margin-bottom: 0!important">
                @foreach($sliders as $item)
                <li>
                  <img src="{{ asset('admin/images/sliders/'.$item->img) }}" alt=""  >
                 </li>
                 @endforeach
            </ul>
        </div>
<div class="site-main">


        <section class="ttm-row about2-section break-1199-colum clearfix">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-5 col-md-12">
                                <!-- ttm_single_image-wrapper -->
                                <div class="ttm_single_image-wrapper mb-5">
                                    <img alt="" class="img-fluid" src="{{asset('web/images/single-img-one.png')}}">
                                    </img>
                                </div>
                                <!-- ttm_single_image-wrapper end -->
                            </div>
                            <div class="col-lg-7 col-md-12">
                                <div class="pt-20">
                                    <div class="row">
                                        <div class="col-lg-8">
                                            <!-- section title -->
                                            <div class="section-title with-desc mt-10 pb-10 clearfix">
                                                <div class="title-header">
                                                    <h5>
                                                        Con&oacute;cenos
                                                    </h5>
                                                    <h2 class="title">
                                                        Acerca de Nosotros
                                                    </h2>
                                                </div>
                                            </div>
                                            <!-- section title end -->
                                        </div>
                                    </div>
                                    <!-- section title -->
                                    <div class="section-title with-desc clearfix">
                                        <div class="title-desc">
                                            <strong>
                                                Colegio de Trabajadores Sociales del Perú - Región II La Libertad - Ancash - Cajamarca
                                            </strong>
                                        </div>
                                    </div>
                                    <!-- section title end -->
                                    <p align="justify">
                                        Somos unas organización profesional de Trabajadores y/o Asistentes sociales de la Región II Sede Trujillo La Libertad Ancash Cajamarca.
                                    </p>
                                    <p align="justify">
                                         Acreditado con Partida Electrónica N° 11313302 SUNARP, constituida sin fines de lucro para colegiar, habilitar y certificar a los Miembros de la Orden y ejercer la representación gremial y defensa del ejercicio legal de la profesión en el marco del estatuto profesional y del Código de Ética y Deontología del Colegio de Trabajadores Sociales del Perú.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <!-- row end -->
                    </div>
                </section>

        <section class="ttm-row blog-section bg-img3 clearfix" style="background-color: #3E4095">
            <div class="container">
                <!-- row -->
                <div class="row">
                    <div class="col-lg-9 col-md-12">
                        <!-- section-title -->
                        <div class="section-title style2 clearfix">
                            <div class="title-header">
                                <h5 style="color:#AAA9D0">Siempre Informando</h5>
                                <h2 class="title" style="color: white">&Uacute;ltimos Eventos</h2>
                            </div>

                        </div><!-- section-title end -->
                    </div>
                    <div style="margin-left: 150px">
                            <a href="{{route('eventos')}}" class="submit ttm-btn ttm-btn-size-md ttm-btn-shape-square ttm-btn-style-border ttm-btn-color-black"  style="color:#fff;background-color: #00BBDC">Ver Más</a></div>
                </div>
                <div class="row">
                    <div class="post-slide owl-carousel owl-theme owl-loaded" data-item="3" data-nav="false" data-dots="false" data-auto="true">
                        @foreach($eventos as $key=>$item)
                        <div class="featured-imagebox featured-imagebox-post">
                            <div class="featured-thumbnail">
                                <a href="{{route('detalleevento',$item->id)}}"><img class="img-fluid" src="{{ asset('admin/images/eventos/'.$item->img) }}" alt=""></a>
                            </div>
                            <div class="featured-content featured-content-post">
                                <div class="post-meta">
                                    <span class="ttm-meta-line"><i class="fa fa-calendar"></i>{{$item->created_at->toFormattedDateString()}}</span>
                                </div>
                                <div class="post-title featured-title">
                                    <h5><a href="{{route('detalleevento',$item->id)}}">{{$item->nombre}}</a></h5>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </section>
</div>
@endsection
