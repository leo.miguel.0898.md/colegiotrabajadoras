@extends('web.layouts.layout')
@section('content')
<div class="ttm-page-title-row">
            <div class="ttm-page-title-row-bg-layer ttm-bg-layer"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12"> 
                        <div class="title-box ttm-textcolor-white">
                            <div class="page-title-heading">
                                <h1 class="title">Consejo Directivo</h1>
                            </div><!-- /.page-title-captions -->
                            <div class="breadcrumb-wrapper">
                                <span>
                                    <a title="Homepage" href="{{route('/')}}"><i class="ti ti-home"></i></a>
                                </span>
                                <span class="ttm-bread-sep">&nbsp; / &nbsp;</span>
                                <span><span>Consejo</span></span>
                            </div>  
                        </div>
                    </div><!-- /.col-md-12 -->  
                </div><!-- /.row -->  
            </div><!-- /.container -->                      
        </div><!-- page-title end-->

    <!--site-main start-->
    <div class="site-main">

        <!-- sidebar -->
        <div class="sidebar ttm-sidebar-left ttm-bgcolor-white break-991-colum clearfix">
            <div class="container">
                <!-- row -->
                <div class="row">
                    <div class="col-lg-3 widget-area sidebar-left ttm-col-bgcolor-yes ttm-bg ttm-left-span ttm-bgcolor-grey">
                        <div class="ttm-col-wrapper-bg-layer ttm-bg-layer"></div>
                        <aside class="widget widget-nav-menu">
                            <ul class="widget-menu">
                                <li><a href="home-maintainance.html"> Home Maintainance </a></li>
                                <li class="active"><a href="painting-services.html"> Painting Services </a></li>
                                <li><a href="renovation-and-painting.html"> Renovation and Painting </a></li>
                                <li><a href="air-conditioner.html"> Air Conditioner </a></li>
                                <li><a href="wiring-and-installation.html"> Wiring and installation </a></li>
                                <li><a href="plumber-services.html"> Plumber Services </a></li>
                            </ul>
                        </aside>
                        <aside class="widget contact-widget">
                            <h3 class="widget-title">Get in touch</h3>      
                            <ul class="contact-widget-wrapper">
                                <li><i class="fa fa-map-marker"></i>1212 Paint Valley Road East Rutherford, New York 06192, USA</li>
                                <li><i class="fa fa-envelope-o"></i><a href="mailto:info@example.com" target="_blank">info@example.com</a></li>
                                <li><i class="fa fa-phone"></i>(+01) 123 456 7890</li>
                                <li><i class="ti ti-alarm-clock"></i>Mon - Sat 8.00 - 18.00. Sunday CLOSED</li>
                            </ul>
                        </aside>
                        <aside class="widget widget_media_image">
                            <a href="#"><img class="img-fluid" src="images/widget-banner.jpg" alt="widget-banner"></a>
                        </aside>
                        <aside class="widget tagcloud-widget">
                            <h3 class="widget-title">Tags</h3>
                            <div class="tagcloud">
                                <a href="#" class="tag-cloud-link">Design</a>
                                <a href="#" class="tag-cloud-link">Dry Wall</a>
                                <a href="#" class="tag-cloud-link">Electrical</a>
                                <a href="#" class="tag-cloud-link">Furniture</a>
                                <a href="#" class="tag-cloud-link">Handywork</a>
                                <a href="#" class="tag-cloud-link">Painting</a>
                                <a href="#" class="tag-cloud-link">Pools</a>
                                <a href="#" class="tag-cloud-link">Roofing</a>
                            </div>
                        </aside>
                    </div>
                    <div class="col-lg-9 content-area">
                        <!-- ttm-service-single-content-are -->
                        <div class="ttm-service-single-content-area">
                            <!-- section title -->
                            <div class="section-title without-sep-line clearfix">
                                <div class="title-header">
                                    <h5>WE BUILD EVERYTHING</h5>
                                    <h2 class="title">Painting Services</h2>
                                </div>
                            </div><!-- section title end -->
                            <div class="ttm_single_image-wrapper mb-35">
                                <img class="img-fluid" src="images/blog/blog-one-1200x800.jpg" alt="">
                            </div>
                            <div class="ttm-service-description">
                                <h4>About Painting Services</h4>
                                <div class="mb-35">
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages.</p>
                                    <p>There re many variaions of passags of Lorem Ipsum available, but majority have suffeed alteration in some , by injectd humour, or randomised ws which ly believable.</p>
                                </div>
                            </div>
                            <div class="sep_holder_box width-100">
                                <span class="sep_holder m-0 mb-35"><span class="sep_line"></span></span>
                                <span class="sep_holder m-0 mb-35"><span class="sep_line"></span></span>
                            </div>
                            <div class="row">
                                <div class="col-sm-8">
                                    <div class="ttm-service-description">
                                        <h4>Fully Trained Employees</h4>
                                        <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accuswqo doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo et inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur ab ilo loream inventore veritatis et quasi architecto beatae vitae dicta sunt sed ut the perspiciatis unde omnis iste natus error sit voluptatem totam ipsum.</p>
                                        <a class="ttm-btn ttm-btn-size-md ttm-btn-shape-square ttm-btn-style-border ttm-btn-color-skincolor mt-20" href="#">Read More</a>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="ttm_single_image-wrapper mb-35">
                                        <img class="img-fluid" src="images/team-member/team-img01.jpg" alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="sep_holder_box width-100">
                                <span class="sep_holder m-0 mb-35"><span class="sep_line"></span></span>
                                <span class="sep_holder m-0 mb-35"><span class="sep_line"></span></span>
                            </div>
                            <div class="row pb-60">
                                <div class="col-sm-4">
                                    <div class="ttm_single_image-wrapper mb-35">
                                        <img class="img-fluid" src="images/team-member/team-img06.jpg" alt="">
                                    </div>
                                </div>
                                <div class="col-sm-8">
                                    <div class="ttm-service-description">
                                        <h4>Quality Paints Every Time</h4>
                                        <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accuswqo doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo et inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur ab ilo loream inventore veritatis et quasi architecto beatae vitae dicta sunt sed ut the perspiciatis unde omnis iste natus error sit voluptatem totam ipsum.</p>
                                        <a class="ttm-btn ttm-btn-size-md ttm-btn-shape-square ttm-btn-style-border ttm-btn-color-skincolor mt-20" href="#">Read More</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- ttm-service-single-content-are end -->
                    </div>
                </div><!-- row end -->
            </div>
        </div>
        <!-- sidebar end -->


    </div>
    @endsection