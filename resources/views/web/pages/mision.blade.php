@extends('web.layouts.layout')
@section('content')
 <div class="ttm-page-title-row">
            <div class="ttm-page-title-row-bg-layer ttm-bg-layer"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="title-box ttm-textcolor-white">
                            <div class="page-title-heading">
                                <h1 class="title">Acerca de Nosotros</h1>
                            </div><!-- /.page-title-captions -->
                            <div class="breadcrumb-wrapper">
                                <span>
                                    <a title="Homepage" href="{{route('/')}}"><i class="ti ti-home"></i></a>
                                </span>
                                <span class="ttm-bread-sep">&nbsp; / &nbsp;</span>
                                <span><span>Nosotros</span></span>
                            </div>
                        </div>
                    </div><!-- /.col-md-12 -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </div>

        <div class="site-main">
                <section class="ttm-row about3-section">
            <div class="container">
                <div class="row">
                    <div class="col-lg-5 col-sm-12">
                        <!-- ttm_single_image-wrapper -->
                        <div class="ttm_single_image-wrapper mb-35">
                            <img class="img-fluid" src="{{asset('web/images/single-img-one.png')}}" alt="">
                        </div><!-- ttm_single_image-wrapper end -->
                    </div>
                    <div class="col-lg-7 col-sm-12">
                        <div class="pl-20">
                            <!-- section title -->
                            <div class="section-title clearfix">
                                <div class="title-header">
                                    <h2>Misión</h2>
                                </div>
                            </div><!-- section title end -->
                            <p align="justify">Somos unas organización profesional de Trabajadores y/o Asistentes sociales de la Región II Sede Trujillo La Libertad Ancash Cajamarca, acreditado con Partida Electrónica N° 11313302 SUNARP, constituida sin fines de lucro para colegiar, habilitar y certificar a los Miembros de la Orden y ejercer la representación gremial y defensa del ejercicio legal de la profesión en el marco del estatuto profesional y del Código de Ética y Deontología del Colegio de Trabajadores Sociales del Perú.></p>
                            <br>
                            <div class="section-title clearfix">
                                <div class="title-header">
                                    <h2>Visión</h2>
                                </div>
                            </div>
                            <p class="mb-30" align="justify">Ser un colegio profesional líder, participativo y transparente, con una cultura organizacional de excelencia que trabaja por el desarrollo humano, articulando proyectos de desarrollo personal, profesional e institucional dentro del arco del desarrollo regional.</p>

                        </div>
                    </div>
                </div><!-- row end-->
            </div>
        </section>
        <!-- about-section end -->

        <!--services-box-section-->

        </div>
@endsection
