@extends('web.layouts.layout')
@section('content')
 <div class="ttm-page-title-row">
            <div class="ttm-page-title-row-bg-layer ttm-bg-layer"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="title-box ttm-textcolor-white">
                            <div class="page-title-heading">
                                <h1 class="title">Nuestra Historia</h1>
                            </div><!-- /.page-title-captions -->
                            <div class="breadcrumb-wrapper">
                                <span>
                                    <a title="Homepage" href="{{route('/')}}"><i class="ti ti-home"></i></a>
                                </span>
                                <span class="ttm-bread-sep">&nbsp; / &nbsp;</span>
                                <span><span>Historia</span></span>
                            </div>
                        </div>
                    </div><!-- /.col-md-12 -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </div>

        <div class="site-main">
                <section class="ttm-row about3-section">
            <div class="container">
                <div class="row">
                    <div class="col-lg-5 col-sm-12">
                        <!-- ttm_single_image-wrapper -->
                        <div class="ttm_single_image-wrapper mb-35">
                            <img class="img-fluid" src="{{asset('web/images/single-img-one.png')}}" alt="">
                        </div><!-- ttm_single_image-wrapper end -->
                    </div>
                    <div class="col-lg-7 col-sm-12">
                        <div class="pl-20">
                            <!-- section title -->
                            <div class="section-title clearfix">
                                <div class="title-header">
                                    <h2>Quiénes Somos</h2>
                                </div>
                            </div><!-- section title end -->
                            <p>El Colegio de Trabajadores Sociales del Perú Región II La Libertad Sede Trujillo, es una institución gremial autónoma, de derecho público interno, sin fines de lucro, que se gesta como una Región descentralizada del Colegio de Trabajadores Sociales del Perú. Como colectivo organizado, se desea aportar el fortalecimiento de la institucionalidad y gobernabilidad democrática con iniciativas técnico-profesionales y ciudadanas.</p>
                            <br>
                            <div class="section-title clearfix">
                                <div class="title-header">
                                    <h2>Reseñas</h2>
                                </div>
                            </div>
                            <p class="mb-30">La primera Escuela de Servicio Social, fue creada el 30 de abril de 1937, Al iniciarse esta carrera en las universidades se otorgaron el título de Asistente Social, posteriormente de Licenciado(a) en Servicio Social y, hoy las universidades otorgan el título de Trabajador(a) Social. El Trabajo Social en nuestra región ha alcanzado un alto grado de desarrollo así como en el Perú, sus actividades profesionales van desde la investigación, la construcción- ejecución- evaluación y vigilancia de las políticas sociales hasta la asistencia social como derecho de sectores excluidos y vulnerables. El 25 de julio se celebra la creación del Colegio de Trabajadores Sociales, mediante Ley 27918 en sustitución, del Colegio de Asistentes Sociales del Perú. Por tanto se celebra el DÍA DEL TRABAJADOR (A) SOCIAL.</p>

                        </div>
                    </div>
                </div><!-- row end-->
            </div>
        </section>
        <!-- about-section end -->

        <!--services-box-section-->

        </div>
@endsection
