@extends('web.layouts.layout')
@section('content')
 <div class="ttm-page-title-row">
            <div class="ttm-page-title-row-bg-layer ttm-bg-layer"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="title-box ttm-textcolor-white">
                            <div class="page-title-heading">
                                <h1 class="title">Tr&aacute;mites</h1>
                            </div><!-- /.page-title-captions -->
                            <div class="breadcrumb-wrapper">
                                <span>
                                    <a title="Homepage" href="{{route('/')}}"><i class="ti ti-home"></i></a>
                                </span>
                                <span class="ttm-bread-sep">&nbsp; / &nbsp;</span>
                                <span><span>Tr&aacute;mites</span></span>
                            </div>
                        </div>
                    </div><!-- /.col-md-12 -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </div>

        <section class="element-row element-style pb-15">
            <div class="container">

                <div class="row mt-50">
                    <div class="portfolio-slide owl-carousel owl-theme owl-loaded" data-item="3" data-nav="false" data-dots="false" data-auto="false">
                         <!-- featured-imagebox -->
                         @foreach($tramites as $key=>$item)
                        <div class="featured-imagebox featured-imagebox-portfolio ttm-box-view-top-image">
                            <div class="ttm-box-view-content-inner">
                                <!-- featured-thumbnail -->
                                <div class="featured-thumbnail">
                                    <a href="{{route('detalletramite',$item->id)}}"> <img class="img-fluid" src="{{ asset('admin/images/tramites/'.$item->img_portada) }}" alt="image"></a>
                                </div><!-- featured-thumbnail end -->
                                <!-- 
                                <div class="ttm-box-view-overlay">
                                    <div class="featured-iconbox ttm-media-link">
                                        <a class="ttm_prettyphoto ttm_image" title="" data-rel="prettyPhoto" href="{{asset('web/images/portfolio/colegiatura.jpg')}}"><i class="ti ti-search"></i></a>
                                        <a href="#" class="ttm_link"><i class="ti ti-link"></i></a>
                                    </div>
                                </div>-->
                            </div>
                            <div class="ttm-box-bottom-content featured-content-portfolio box-shadow2">
                                <h2 class="featured-title"><a href="{{route('detalletramite',$item->id)}}">{{$item->nombre}}</a></h2>
                            </div>
                        </div>
                        @endforeach<!-- featured-imagebox -->
                    </div>
                </div>
            </div>
        </section>
@endsection
