@extends('web.layouts.layout')
@section('content')
<div class="ttm-page-title-row">
            <div class="ttm-page-title-row-bg-layer ttm-bg-layer"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="title-box ttm-textcolor-white">
                            <div class="page-title-heading">
                                <h1 class="title">Comunicados</h1>
                            </div><!-- /.page-title-captions -->
                            <div class="breadcrumb-wrapper">
                                <span>
                                    <a title="Homepage" href="{{route('/')}}"><i class="ti ti-home"></i></a>
                                </span>
                                <span class="ttm-bread-sep">&nbsp; / &nbsp;</span>
                                <span><span>Comunicados</span></span>
                            </div>
                        </div>
                    </div><!-- /.col-md-12 -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </div>
        <div class="site-main">

        <div class="ttm-row pb-70 ttm-bgcolor-grey clearfix">
            <div class="container">
                <div class="row">
                    @foreach($comunicados as $key=>$item)
                    <div class="col-md-4 col-sm-6">
                        <!-- featured-imagebox-post -->

                        <div class="featured-imagebox featured-imagebox-post">
                            <div class="featured-thumbnail">
                                <a href="{{route('detallecomunicado',$item->id)}}"><img class="img-fluid" src="{{ asset('admin/images/comunicados/'.$item->img) }}" alt=""></a>
                            </div>
                            <div class="featured-content featured-content-post">
                                <div class="post-meta">
                                    <span class="ttm-meta-line"><i class="fa fa-calendar"></i><a href="#">{{$item->created_at->toFormattedDateString()}}</a></span>
                                </div>
                                <div class="post-title featured-title">
                                    <h5><a href="{{route('detallecomunicado',$item->id)}}">{{$item->nombre}}</a></h5>
                                </div>
                            </div>
                        </div><!-- featured-imagebox-post end -->

                    </div>
                    @endforeach
                    <div class="col-lg-12">
                        <div class="ttm-pagination">
                            <span class="page-numbers current">1</span>
                            <a class="page-numbers" href="#">2</a>
                            <a class="next page-numbers" href="#"><i class="ti ti-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
@endsection
