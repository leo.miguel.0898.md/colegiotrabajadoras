@extends('web.layouts.layout')
@section('content')
<div class="ttm-page-title-row">
            <div class="ttm-page-title-row-bg-layer ttm-bg-layer"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="title-box ttm-textcolor-white">
                            <div class="page-title-heading">
                                <h1 class="title">Detalle Tramites</h1>
                            </div><!-- /.page-title-captions -->
                            <div class="breadcrumb-wrapper">
                                <span>
                                    <a title="Homepage" href="{{route('/')}}"><i class="ti ti-home"></i></a>
                                </span>
                                <span class="ttm-bread-sep">&nbsp; / &nbsp;</span>
                                <span><span>Detalles</span></span>
                            </div>
                        </div>
                    </div><!-- /.col-md-12 -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </div>
        <div class="site-main single">

        <!-- sidebar -->
        <div class="sidebar ttm-sidebar-right ttm-bgcolor-white break-991-colum clearfix">
            <div class="container">
                <!-- row -->
                <div class="row">
                    <div class="col-lg-9 content-area">
                        <!-- post -->
                        <article class="post ttm-blog-classic">
                            <!-- post-featured-wrapper -->
                            <div class="post-featured-wrapper">
                                <div class="post-featured">
                                    <img class="img-fluid" src="{{ asset('admin/images/tramites/'.$tramites->img) }}" alt="">
                                </div>
                                <div class="ttm-box-post-date">
                                    <span class="ttm-entry-date">
                                        <time class="entry-date">{{Carbon::parse($tramites->created_at)->format('d')}}<span class="entry-month entry-year">{{$tramites->created_at->format('M')}}</span></time>
                                    </span>
                                </div>
                            </div>
                            <!-- post-featured-wrapper end -->
                            <!-- ttm-blog-classic-box-content -->
                            <div class="ttm-blog-classic-box-content">
                                <div class="entry-content">
                                    <div class="ttm-entry-meta-wrapper">

                                    </div>
                                    <div class="ttm-box-desc-text">
                                        <h2>{{$tramites->nombre}}</h2>
                                        <p>{{$tramites->descripcion}}</p>
                                        @if($tramites->id=='1')
                                        <a href="{{ asset('admin/word/constancia_habilidad_2020.docx') }}" class="nav-link">Click aqui para Descargar Constancia de Habilitación</a>
                                        @elseif($tramites->id=='2')
                                        <a href="{{ asset('admin/word/requisitos_carnet_2020.docx') }}" class="nav-link">Click aqui para Descargar los Requisitos para el Carnet</a>
                                        @elseif($tramites->id=='3')
                                        <a href="{{ asset('admin/word/requisitos_vigentes_2020.docx') }}" class="nav-link">Click aqui para Descargar los Requisitos para la Colegiatura</a>
                                        @endif
                                </div>
                            </div> <!-- ttm-blog-classic-box-content end -->
                        </article><!-- post end -->
                    </div>
                    <div class="col-lg-3 widget-area sidebar-right ttm-col-bgcolor-yes ttm-bg ttm-right-span ttm-bgcolor-grey">
                        <div class="ttm-col-wrapper-bg-layer ttm-bg-layer"></div>

                        <aside class="widget widget-recent-post">
                            <h3 class="widget-title">Ultimos Tramites</h3>
                            <ul class="ttm-recent-post-list">
                                @foreach($tramite as $key=>$item)
                                <li class="ttm-recent-post-list-li clearfix">
                                    <a href="{{route('detalletramite',$item->id)}}"><img class="img-fluid" src="{{ asset('admin/images/tramites/'.$item->img) }}"></a>
                                    <span class="post-date">{{Carbon::parse($item->created_at)->format('M')}}&nbsp{{Carbon::parse($item->created_at)->format('d')}}, &nbsp{{Carbon::parse($item->created_at)->format('Y')}}</span>
                                    <a href="{{route('detalletramite',$item->id)}}">{{$item->nombre}}</a>
                                </li>
                                @endforeach
                            </ul>
                        </aside>
                    </div>
                </div><!-- row end -->
            </div>
        </div>
        <!-- sidebar end -->

    </div>
@endsection
