@extends('web.layouts.layout')
@section('content')
<div class="site-main" >
    <div class="ttm-row map-section ttm-bgcolor-white">
            <div class="map-wrapper">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3949.7733753917128!2d-79.04219779695892!3d-8.124540991830195!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xd0d4f4b8941b6b22!2sColegio%20de%20Trabajadores%20Sociales%20del%20Per%C3%BA%20-%20Regi%C3%B3n%20II!5e0!3m2!1ses!2spe!4v1577457694439!5m2!1ses!2spe" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
            </div>
        </div>
    <br><br>
        <section class="ttm-row pb-160 res-991-pb-100 clearfix">
            <div class="container">
                <div class="row">
                    <div class="col-md-7 pr-60 res-767-pr-15">
                        <!-- section title -->
                        <div class="section-title with-desc clearfix">
                            <div class="title-header">
                                @include('alerts')
                                <h5>Cont&aacute;ctanos</h5>
                                <h2 class="title">Formulario de Contacto</h2>
                            </div>
                            <div class="title-desc">Colegio de Trabajadores Sociales del Perú <br>Región II La Libertad - Ancash - Cajamarca</div>
                        </div><!-- section title end -->

                        <form action="{{route('contacto-mensaje')}}" class="ttm-contactform wrap-form clearfix" method="POST">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-lg-6">
                                    <label>
                                        <span class="text-input"><i class="ttm-textcolor-skincolor ti-user"></i><input name="nombres" type="text" value="" placeholder="Nombre" required="required"></span>
                                    </label>
                                </div>
                                <div class="col-lg-6">
                                    <label>
                                        <span class="text-input"><i class="ttm-textcolor-skincolor ti-mobile"></i><input name="telefono" type="text" value="" placeholder="Celular" required="required"></span>
                                    </label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <label>
                                        <span class="text-input"><i class="ttm-textcolor-skincolor ti-email"></i><input name="email" type="email" value="" placeholder="Email" required="required"></span>
                                    </label>
                                </div>
                                <div class="col-lg-6">
                                    <label>
                                        <span class="text-input"><i class="ttm-textcolor-skincolor ti-key"></i><input name="numero" type="text" value="" placeholder="Número de Colegiatura" required="required"></span>
                                    </label>
                                </div>
                            </div>
                            <label>
                                <span class="text-input"><i class="ttm-textcolor-skincolor ti-comment"></i><textarea name="mensaje" cols="40" placeholder="Mensaje" required="required"></textarea></span>
                            </label>
                            <button type="submit"  class="submit ttm-btn ttm-btn-size-md ttm-btn-shape-square ttm-btn-style-border ttm-btn-color-black"  style="color:#fff">Enviar Mensaje</button>
                        </form>
                    </div>
                    <div class="col-md-5">
                        <div class="ttm-rounded-shadow-box pt-40 pr-50 pb-55 pl-50 box-shadow2 res-767-mt-40 clearfix">
                            <h4>Informaci&oacute;n de contacto</h4>
                            <ul class="ttm_contact_widget_wrapper">
                                <li><i class="ttm-textcolor-skincolor ti ti-location-pin"></i>Calle Las Gaviotas #1140 - Urb. Los Pinos<br></li>
                                <li><i class="ttm-textcolor-skincolor ti ti-mobile"></i>(+51) 949 739 349<br>(+51) 985 422 690<br>(+51) 949 333 883</li>
                                <li><i class="ttm-textcolor-skincolor ti ti-comment"></i><a href="#">ctspregion.lac@gmail.com</a></li>
                                <li><i class="ttm-textcolor-skincolor ti ti-world"></i><a href="http://www.ctspregion2.com" target="_blank">http://www.ctspregion2.com</a></li>
                            </ul>
                            <div class="social-icons circle social-hover">
                                <ul class="list-inline">
                                    <li class="social-facebook"><a class="tooltip-top ttm-textcolor-skincolor" target="_blank" href="https://www.facebook.com/pg/CtspRegionll" target="_blank" data-tooltip="Facebook" style="background-color: #3b5998;border-color:#3b5998 "><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                    <li class="social-twitter"><a class="tooltip-top ttm-textcolor-skincolor" target="_blank" href="https://twitter.com/CtspRegionII" data-tooltip="twitter" style="background-color: #34b7f1;border-color:#34b7f1"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                    <li class="social-instagram"><a class="tooltip-top ttm-textcolor-skincolor" target="_blank" href="https://www.instagram.com/ctsp.regionll/" data-tooltip="Instagram" style="background-color: #C13584;border-color:#C13584"><i class="fa fa-instagram" aria-hidden="true" ></i></a></li>

                                    <li class="social-whatsapp"><a class="tooltip-top ttm-textcolor-skincolor" target="_blank" href="https://api.whatsapp.com/send?phone=51985422690" data-tooltip="whatsapp" style="background-color: #128c7e;border-color:#128c7e"><i class="fa fa-whatsapp" aria-hidden="true"></i></a></li>

                                </ul>
                            </div>
                        </div>
                    </div>
                </div><!-- row end -->
            </div>
        </section>

    </div>
@endsection
