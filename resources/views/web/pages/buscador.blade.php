@extends('web.layouts.layout')
@section('style')
<link rel="stylesheet" href="{{asset('css/estilopopup.css')}}">
<style type="text/css">
    @media only screen and (max-width: 600px) {
        .hugo{
            


        }
        .bot{
            width: 380px!important;
            padding-top: 20px!important;
            padding-left: 95px!important;
            padding-bottom: 15px!important;


        }
        .garcia{
            width: 380px!important;
            padding-top: 15px!important;
            padding-bottom: 15px!important;
        }
    }

    @media  only screen and (min-width: 901px){
        .modal-lg {
            max-width: 898px;
            max-height: 700px;
        }
        .modal-content {
            max-width: 880px;
            max-height: 680px;
        }
        .modal-body {
            padding: 0px;
            padding-left: 0px;
        }
        .imagen{
            background: url('{{asset('web/images/modal.jpg')}}');
            max-width: 900px;
            max-height: 680px;
        }
        .imagencolegiado{
            max-width: 200px;
            max-height: 180px;
        }
        .espacio{
            padding-top: 240px;
            padding-left: 122px;
            padding-bottom: 10px;
        }
        .espacionombres{
            padding-top: 1px;
            line-height: 0.7;
            padding-left: 1px;
        }
        .letraapellidos{
            font-size: 20px;
        }
        .letranombres{
            font-size: 20px;
            padding-bottom: 6px;
        }
        .letratrabajador{
            font-size: 16px;
            padding-left: 5px;
        }
        .info{
            padding-top: 245px;
            padding-left: 38px;
        }
       
        .hyphenation {
             word-break: break-all;
             /* Non standard for webkit */
             word-break: break-word; */
          
        -webkit-hyphens: auto;
           -moz-hyphens: auto;
            -ms-hyphens: auto;
                hyphens: auto;
        }
        .letracodigo{
            font-size: 12px; 
            color: #000000; 
            margin-bottom: 10px;
            line-height: 1.2; width: 215px; height: 10px; 
            padding-left: 75px;
        }
        .espaciodni{
            font-size: 12px; 
            color: #000000; 
            margin-bottom: 10px;
            line-height: 1.2; width: 215px; height: 10px; 
            padding-left: 75px;
            padding-bottom: 32px;
        }
        .espacioemail{
            font-size: 12px;
            color: #000000; 
            margin-bottom: 10px;
            line-height: 1.2; width: 215px; height: 10px; 
            padding-left: 75px;
            padding-bottom: 34px;
        }
        .espaciocelular{
            font-size: 12px;
            color: #000000; 
            margin-bottom: 10px;
            line-height: 1.2; width: 215px; height: 10px; 
            padding-left: 75px;
            padding-bottom: 31px;
        }
        .espaciomaestria{
            font-size: 12px;
            color: #000000; 
            margin-bottom: 10px;
            line-height: 1.2; width: 215px; height: 10px; 
            padding-left: 75px;
            padding-bottom: 34px;
        }
        .espaciodoctorado{
            font-size: 12px;
            color: #000000; 
            margin-bottom: 10px;
            line-height: 1.2; width: 215px; height: 10px; 
            padding-left: 75px;
            padding-bottom: 33px;
        }
        .espacioespecialidad{
            font-size: 12px;
            color: #000000; 
            margin-bottom: 10px;
            line-height: 1.2; width: 215px; height: 10px;
            padding-left: 75px;
        }
        input[type=text].letracodigo{
            font-size: 18px;  
            margin-bottom: 10px;
            line-height: 1.2; width: 180px; 
            padding-right: 14px;
        }
        .letratraconcod{
            line-height: 0.4;
        }
    }
    @media only screen and (max-width: 900px){
        .modal-lg {
            max-width: 700px;
        }
        .modal-content {
            max-width: 648px;
            max-height: 512px;
        }
        .modal-body {
            padding: 0px;
            padding-left: 0px;
        }
        .imagen{
            background: url('{{asset('web/images/modal.jpg')}}');
            max-width: 646px;
            max-height: 510px;
        }
        .imagencolegiado{
            max-width: 100px;
            max-height: 136px;
        }
        .espacio{
            padding-top: 164px;
            padding-left: 69px;
        }
        .espacionombres{
            padding-top: 5px;
            line-height: 0.7;
            padding-right: 20px;
        }
        .letraapellidos{
            font-size: 18px;
        }
        .letranombres{
            font-size: 16px;
            padding-bottom: 2px;
        }
        .letratrabajador{
            font-size: 13px;
            padding-right: 16px;
        }
        
        .info{
            padding-top: 180px;
        }
       
        .hyphenation {
             word-break: break-all;
             /* Non standard for webkit */
             word-break: break-word; */
          
        -webkit-hyphens: auto;
           -moz-hyphens: auto;
            -ms-hyphens: auto;
                hyphens: auto;
        }
        .letracodigo{
            font-size: 10px; 
            color: #000000; 
            margin-bottom: 10px;
            line-height: 1.2; width: 180px; height: 10px; font-size: 10px;
            padding-left: 75px;
        }
        .espaciodni{
            font-size: 10px; 
            color: #000000; 
            margin-bottom: 10px;
            line-height: 1.2; width: 180px; height: 10px; font-size: 10px;
            padding-left: 75px;
            padding-bottom: 20px;
        }
        .espacioemail{
            font-size: 10px;
            color: #000000; 
            margin-bottom: 10px;
            line-height: 1.2; width: 180px; height: 10px; font-size: 10px;
            padding-left: 75px;
            padding-bottom: 23px;
        }
        .espaciocelular{
            font-size: 10px;
            color: #000000; 
            margin-bottom: 10px;
            line-height: 1.2; width: 180px; height: 10px; font-size: 10px;
            padding-left: 75px;
            padding-bottom: 22px;
        }
        .espaciomaestria{
            font-size: 10px;
            color: #000000; 
            margin-bottom: 10px;
            line-height: 1.2; width: 180px; height: 10px; font-size: 10px;
            padding-left: 75px;
            padding-bottom: 21px;
        }
        .espaciodoctorado{
            font-size: 10px;
            color: #000000; 
            margin-bottom: 10px;
            line-height: 1.2; width: 180px; height: 10px; font-size: 10px;
            padding-left: 75px;
            padding-bottom: 23px;
        }
        .espacioespecialidad{
            font-size: 10px;
            color: #000000; 
            margin-bottom: 10px;
            line-height: 1.2; width: 180px; height: 10px; font-size: 10px;
            padding-left: 75px;
        }
        input[type=text].letracodigo{
            font-size: 18px;  
            margin-bottom: 10px;
            line-height: 1.2; width: 180px; 
            padding-right: 34px;
        }
        .letratraconcod{
            line-height: 0.2;
        }
    }

    @media only screen and (max-width: 600px){
        .modal-lg {
            max-width: 500px;
        }
        .modal-content {
            max-width: 490px;
            max-height: 410px;
        }
        .modal-body {
            padding: 0px;
            padding-left: 0px;
        }
        .imagen{
            background: url('{{asset('web/images/modal.jpg')}}');
            max-width: 490px;
            max-height: 410px;
        }
        .imagencolegiado{
            max-width: 77px;
            max-height: 110px;
            padding-top: 2px
        }
        .espacio{
            padding-top: 120px;
            padding-left: 40px;
        }
        .espacionombres{
            padding-top: 3px;
            line-height: 0.7;
            padding-right: 18px;
        }
        .letraapellidos{
            font-size: 13px;
        }
        .letranombres{
            font-size: 12px;
            margin-bottom: 0px;
        }
        .letratrabajador{
            font-size: 11px;
            padding-right: 20px
        }
        .letratraconcod{
            line-height: 0.2;
        }

        .info{
            padding-top: 144px;
        }
       
        .hyphenation {
             word-break: break-all;
             /* Non standard for webkit */
             word-break: break-word; */
          
        -webkit-hyphens: auto;
           -moz-hyphens: auto;
            -ms-hyphens: auto;
                hyphens: auto;
        }
        input[type=text].letracodigo{
            font-size: 15px;  
            margin-bottom: 10px;
            line-height: 1.2; width: 180px; 
            padding-right: 33px;
        }
        .espaciodni{
            font-size: 9px; 
            color: #000000; 
            margin-bottom: 4px;
            line-height: 1.2; width: 148px; height: 10px;
            padding-left: 64px;
            padding-bottom: 20px;
        }
        .espacioemail{
            font-size: 9px;
            color: #000000; 
            margin-bottom: 5px;
            line-height: 1.2; width: 148px; height: 10px;
            padding-left: 64px;
            padding-bottom: 23px;
        }
        .espaciocelular{
            font-size: 9px;
            color: #000000; 
            margin-bottom: 2px;
            line-height: 1.2; width: 148px; height: 10px;
            padding-left: 64px;
            padding-bottom: 22px;
        }
        .espaciomaestria{
            font-size: 9px;
            color: #000000; 
            margin-bottom: 6px;
            line-height: 1.2; width: 148px; height: 10px;
            padding-left: 64px;
            padding-bottom: 21px;
        }
        .espaciodoctorado{
            font-size: 9px;
            color: #000000; 
            margin-bottom: 2px;
            line-height: 1.2; width: 148px; height: 10px;
            padding-left: 64px;
            padding-bottom: 23px;
        }
        .espacioespecialidad{
            font-size: 9px;
            color: #000000; 
            margin-bottom: 10px;
            line-height: 1.2; width: 148px; height: 10px;
            padding-left: 64px;
        }
    }
    @media only screen and (max-width: 415px){
        .modal-lg {
            max-width: 500px;
        }
        .modal-content {
            max-width: 320px;
            max-height: 250px;
        }
        .modal-body {
            padding: 0px;
            padding-left: 0px;
        }
        .imagen{
            max-width: 320px;
            max-height: 250px;
        }

    }

</style>
<style>
    .btn-close-popup{
    width: 45px;
    height: 45px;
    position: absolute;
    right: -30px;
    top: -25px;
    padding: 5px;
    background: #575AC5;
    color: white;
    border-radius: 50%;
    line-height: 10px;
}
 <style>
   
    p.large {
      line-height: 1;
    }
</style>

</style>
@endsection
@section('content')
<div class="ttm-page-title-row" style="z-index: 1">
    <div class="ttm-page-title-row-bg-layer ttm-bg-layer"></div>
         <div class="container">
             <div class="row">
                 <div class="col-md-12">
                        <div class="title-box ttm-textcolor-white">
                            <div class="page-title-heading">
                                <h1 class="title">Colegiatura</h1>
                            </div><!-- /.page-title-captions -->

                        </div>
                    </div><!-- /.col-md-12 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div>
<div class="site-main">
    <section class="ttm-row row-top-section clearfix">
        <div class="container">
            <form>
                <div class="row">
                    <div class="col-lg-12 ">
                        <div class="mt_40 mlr-30 res-767-mt-0 ">
                            <div class="row row-equal-height ttm-bgcolor-white" style="-webkit-box-shadow: 0px 2px 9px 6px rgba(0,0,0,0.08); -moz-box-shadow: 0px 2px 9px 6px rgba(0,0,0,0.08); box-shadow: 0px 2px 9px 6px rgba(0,0,0,0.08);" >
                                <div class="col-2.5 ">
                                    <!-- featured-icon-box -->
                                    <div class="featured-icon-box style1 top-icon text-center garcia" >
                                        <input type="" name="ctsp" class="field searchform-s" placeholder="   Nro colegiatura" style="background-color:#F8F9FB;height:48px;width:190px; color:#AAA9D0; border:white" title="Solo ingrese números">
                                    </div>
                                    
                                </div>
                                <div class="col-2.5 ">
                                    <!-- featured-icon-box -->
                                    <div class="featured-icon-box style1  text-center garcia">
                                        <span class="text-input"><input type="" name="nombres" placeholder="    Nombres" style="background-color:#F8F9FB;height:48px;width:190px; color:#AAA9D0; border:white" title="Solo ingrese letras">
                                    </div>
                                </div>
                                <div class="col-2.5 ">
                                    <!-- featured-icon-box -->
                                    <div class="featured-icon-box style1 top-icon text-center garcia">
                                        <input type="" name="paterno" class="field searchform-s" placeholder="   Apellido paterno" style="background-color:#F8F9FB;height:48px;width:190px; color:#AAA9D0; border:white" title="Solo ingrese letras">
                                    </div>
                                </div>
                                <div class="col-2.5 " >
                                    <!-- featured-icon-box -->
                                    <div class="featured-icon-box style1 top-icon text-center garcia">
                                        <input type="" name="materno" class="field searchform-s" placeholder="   Apellido materno" style="background-color:#F8F9FB;height:48px;width:190px; color:#AAA9D0; border:white" title="Solo ingrese letras">
                                    </div>
                                </div>
                                <div class="col-2 featured-icon-box style1 top-icon text-center bot" style="height:70px;">
                                        <button class="" href="#" style="background-color: #575AC5;" name="buscar" id="buscar"><i class="ti ti-search"></i>&nbspBuscar</button>
                               <!-- featured-icon-box end-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>  
            </br>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr style="color:#fff;background-color: #575AC5;">
                                        <th class="product-remove">C&Oacute;DIGO</th>
                                        <th class="product-thumbnail">NOMBRES Y APELLIDOS</th>
                                        <th class="product-name">ESTADO</th>
                                        <th class="product-price">FOTO</th>
                                        <th class="product-quantity">DETALLE</th>
                                    </tr>
                                </thead>
                                @php
                                    if(isset($_GET['buscar'])) {
                                @endphp
                                <tbody>
                                    @foreach($clientes as $key=>$cliente)
                                    <tr>
                                        <td>
                                            {!! $cliente->ctsp !!}
                                        </td>
                                        <td>
                                            {!! $cliente->nombres !!} {!! $cliente->paterno !!} {!! $cliente->materno !!}
                                        </td>
                                        @if($cliente->estado=='habilitado')
                                        <td>
                                            <img src="{{ asset('web/images/habilitado.png') }}">
                                        </td>
                                        @else
                                        <td>
                                            <img src="{{ asset('web/images/inhabilitado.png') }}">
                                        </td>
                                        @endif
                                        <td>
                                            <!--<img src="{{ asset('admin/images/clientes/'.$cliente->img) }}" class="img-fluid">-->
                                            @if($cliente->img==null)
                                            <img src="{{asset('web/images/user.jpg')}}" style="width: 40px; height: 50px; background-color: #575AC5;">
                                            @else
                                            <img src="{{ asset('admin/images/clientes/'.$cliente->img) }}" style="width: 40px; height: 50px; background-color: #575AC5;">
                                            @endif
                                        </td>
                                        <td class="product-quantity" data-title="Quantity">
                                            <!--
                                            <button type="button" data-toggle="modal" data-target="#exampleModalCenter{{ $key + 1 }}" style="background-color: #00BBDC;"><i class="fa fa-eye"></i> Detalle

                                            </button>
                                            <a href="#" class="miModal btn btn-warning btn-sm" data-id="{{$cliente->ctsp}}" data-title="{{$cliente->nombres}}" data-body="{{$cliente->paterno}}">
                                                <i class="glyphicon glyphicon-pencil"></i>
                                            </a>
                                            <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#miModal{{ $cliente->ctsp }}">Abrir Modal</button>
                                            -->
                                            
                                            <button type="button" data-toggle="modal" data-id="{{$cliente->ctsp}}" data-name="{{$cliente->nombres}}" data-name="{{$cliente->nombres}}" data-paterno="{{$cliente->paterno}}" data-materno="{{$cliente->materno}}" data-maestria="{{$cliente->maestria}}" data-doctorado="{{$cliente->doctorado}}" data-especialidad="{{$cliente->especialidad}}"  data-genero="{{$cliente->genero}}" data-email="{{$cliente->email}}" data-dni="{{$cliente->dni}}" data-celular="{{$cliente->celular}}" data-img="{{$cliente->img}}" data-imagen="{{ asset('admin/images/clientes/'.$cliente->img) }}" title="Add this item" class="edit" href="#addBookDialog" style="background-color: #00BBDC;"><i class="fa fa-eye"></i> Detalle</button>

                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                                @php
                                    }
                                @endphp
                            </table>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </section>
</div>


@endsection





@section('script')

@endsection

