<header id="masthead" class="header ttm-header-style-classic"><meta charset="gb18030">
            <!-- ttm-topbar-wrapper -->
            <div class="ttm-topbar-wrapper ttm-bgcolor-darkgrey ttm-textcolor-white clearfix">
                <div class="container">
                    <div class="ttm-topbar-content">
                        <ul class="top-contact ttm-highlight-left text-left">
                            <li><i class="fa fa-phone"></i><strong>Contacto:</strong> <span class="tel-no"> (+51) 985 422 690</span></li>
                        </ul>
                        <div class="topbar-right text-right">
                            <ul class="top-contact">
                                <li><i class="fa fa-envelope-o"></i><strong>Email: </strong><a href="mailto:info@example.com.com">ctspregion.lac@gmail.com</a></li>
                            </ul>
                            <div class="ttm-social-links-wrapper list-inline">
                                <ul class="social-icons">
                                    <li><a href="https://www.facebook.com/pg/CtspRegionll" target="_blank"><i class="fa fa-facebook"></i></a>
                                    </li>
                                    <li><a href="https://twitter.com/CtspRegionII" target="_blank"><i class="fa fa-twitter"></i></a>
                                    <li><a href="https://www.instagram.com/ctsp.regionll/" target="_blank"><i class="fa fa-instagram"></i></a>
                                    <li><a href="https://api.whatsapp.com/send?phone=51985422690"target="_blank"><i class="fa fa-whatsapp"></i></a>

                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- ttm-topbar-wrapper end -->
            <!-- ttm-header-wrap -->
            <div class="ttm-header-wrap">
                <!-- ttm-stickable-header-w -->
                <div id="ttm-stickable-header-w" class="ttm-stickable-header-w clearfix">
                    <div id="site-header-menu" class="site-header-menu">
                        <div class="site-header-menu-inner ttm-stickable-header">
                            <div class="container">
                                <!-- site-branding -->
                                <div class="site-branding">
                                    <a class="home-link" href="{{route('/')}}" rel="home">
                                        <img id="logo-img" class="img-center" src="{{asset('web/images/logo.png')}}" alt="logo-img" >
                                    </a>
                                </div><!-- site-branding end -->
                                <!--site-navigation -->
                                <div id="site-navigation" class="site-navigation">
                                    <div class="ttm-menu-toggle">
                                        <input type="checkbox" id="menu-toggle-form">
                                        <label for="menu-toggle-form" class="ttm-menu-toggle-block">
                                            <span class="toggle-block toggle-blocks-1"></span>
                                            <span class="toggle-block toggle-blocks-2"></span>
                                            <span class="toggle-block toggle-blocks-3"></span>
                                        </label>
                                    </div>
                                    <nav id="menu" class="menu">
                                        <ul class="dropdown">
                                           <li @if(request()->is('/')) class="active" @endif><a href="{{route('/')}}">Inicio</a>
                                            </li>
                                            <li @if(request()->is('mision')) class="active" @endif || @if(request()->is('historia')) class="active" @endif><a href="#">Institución</a>
                                                <ul>
                                                    <li><a href="{{route('historia')}}">Historia</a></li>
                                                    <li><a href="{{route('mision')}}">Misión y Visión</a></li>
                                                </ul>
                                            </li>

                                           <li @if(request()->is('evento')) class="active" @endif || @if(request()->is('detalleevento')) class="active" @endif><a href="#">Eventos</a>
                                                <ul>
                                                    <li><a href="{{route('eventos')}}">Eventos CTSP</a></li>
                                                    <li><a href="{{route('comunicados')}}">Comunicados</a></li>
                                                </ul>
                                            </li>
                                            <li @if(request()->is('tramites')) class="active" @endif><a href="{{route('tramites')}}">Tr&aacute;mites</a>
                                            </li>
                                            <li @if(request()->is('contacto')) class="active" @endif><a href="{{route('contacto')}}">Cont&aacute;ctenos</a>
                                            </li>
                                            <a href="{{route('buscador')}}" class="submit ttm-btn ttm-btn-size-md ttm-btn-shape-square ttm-btn-style-border ttm-btn-color-black" value="Colegiatura" style="color:#fff;background-color: #575AC5">COLEGIATURA</a>
                                        </ul>
                                    </nav>
                                </div><!-- site-navigation end-->
                            </div>
                        </div>
                    </div>
                </div><!-- ttm-stickable-header-w end-->
            </div><!--ttm-header-wrap end -->
        </header>
