<!DOCTYPE html>
<html lang="en">
<!-- Copied from http://themetechmount.com/html/boldman/index.html by Cyotek WebCopy 1.7.0.600, jueves, 12 de diciembre de 2019, 10:30:01 p.m -->
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="keywords" content="PYRUS STUDIO">
<meta name="description" content="PYRUS STUDIO">
<meta name="author" content="https://www.pyrusstudio.com/">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<title>CTSP</title>

<!-- favicon icon -->
<link rel="shortcut icon" href="{{asset('web/images/favicon.png')}}">

<!-- bootstrap -->
<link rel="stylesheet" type="text/css" href="{{asset('web/css/bootstrap.min.css')}}">

<!-- animate -->
<link rel="stylesheet" type="text/css" href="{{asset('web/css/animate.css')}}">

<!-- owl-carousel -->
<link rel="stylesheet" type="text/css" href="{{asset('web/css/owl.carousel.css')}}">

<!-- fontawesome -->
<link rel="stylesheet" type="text/css" href="{{asset('web/css/font-awesome.css')}}">

<!-- themify -->
<link rel="stylesheet" type="text/css" href="{{asset('web/css/themify-icons.css')}}">

<!-- flaticon -->
<link rel="stylesheet" type="text/css" href="{{asset('web/css/flaticon.css')}}">


<!-- REVOLUTION LAYERS STYLES -->

    <link rel="stylesheet" type="text/css" href="{{asset('web/revolution/css/layers.css')}}">

    <link rel="stylesheet" type="text/css" href="{{asset('web/revolution/css/settings.css')}}">

<!-- prettyphoto -->
<link rel="stylesheet" type="text/css" href="{{asset('web/css/prettyPhoto.css')}}">

<!-- shortcodes -->
<link rel="stylesheet" type="text/css" href="{{asset('web/css/shortcodes.css')}}">

<!-- main -->
<link rel="stylesheet" type="text/css" href="{{asset('web/css/main.css')}}">

<!--Color Switcher Mockup-->
<link rel="stylesheet" type="text/css" href="{{asset('web/css/color-switcher.css')}}">

<!--Color Themes-->
<link id="switcher-color" href="{{asset('web/css/colors/default-color.css')}}" rel="stylesheet">

<!-- responsive -->
<link rel="stylesheet" type="text/css" href="{{asset('web/css/responsive.css')}}">


</head>
<a id="totop" href="#top">
        <i class="fa fa-angle-up"></i>
    </a>
<body>
    @yield('style')
    <!--page start-->
    <div class="page">

        <!--
        <div id="preloader">
          <div id="status">&nbsp;</div>
        </div> -->

        <!--header start-->
        @include('web.layouts.header')

        <!--header end-->

        @yield('content')
       <!-- END REVOLUTION SLIDER -->

    <!--site-main start-->
    <!--site-main end-->


    <!--footer start-->
    @include('web.layouts.footer')

    @yield('script')
    <!--footer end-->
    <!--back-to-top start-->

    <!--back-to-top end-->

    <!-- Color Palate / Color Switcher -->
    <!-- Color Palate / Color Switcher end-->

</div><!-- page end -->


    <!-- Javascript -->

    <script src="{{asset('web/js/jquery.min.js')}}"></script>
    <script src="{{asset('web/js/tether.min.js')}}"></script>
    <script src="{{asset('web/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('web/js/color-switcher.js')}}"></script>
    <script src="{{asset('web/js/jquery.easing.js')}}"></script>
    <script src="{{asset('web/js/jquery-waypoints.js')}}"></script>
    <script src="{{asset('web/js/jquery-validate.js')}}"></script>
    <script src="{{asset('web/js/owl.carousel.js')}}"></script>
    <script src="{{asset('web/js/jquery.prettyPhoto.js')}}"></script>
    <script src="{{asset('web/js/numinate.min.js?ver=4.9.3')}}"></script>
    <script src="{{asset('web/js/main.js')}}"></script>

    <!-- Revolution Slider -->
    <script src="{{asset('web/revolution/js/jquery.themepunch.tools.min.js')}}"></script>
    <script src="{{asset('web/revolution/js/jquery.themepunch.revolution.min.js')}}"></script>
    <script src="{{asset('web/revolution/js/slider.js')}}"></script>


    <!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->

    <script src="{{asset('web/revolution/js/extensions/revolution.extension.actions.min.js')}}"></script>
    <script src="{{asset('web/revolution/js/extensions/revolution.extension.carousel.min.js')}}"></script>
    <script src="{{asset('web/revolution/js/extensions/revolution.extension.kenburn.min.js')}}"></script>
    <script src="{{asset('web/revolution/js/extensions/revolution.extension.layeranimation.min.js')}}"></script>
    <script src="{{asset('web/revolution/js/extensions/revolution.extension.migration.min.js')}}"></script>
    <script src="{{asset('web/revolution/js/extensions/revolution.extension.navigation.min.js')}}"></script>
    <script src="{{asset('web/revolution/js/extensions/revolution.extension.parallax.min.js')}}"></script>
    <script src="{{asset('web/revolution/js/extensions/revolution.extension.slideanims.min.js')}}"></script>
   
<!--
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-3.4.1.min.js"></script>
<script type="text/javascript" src="js/bootstrap.js"></script>
  -->
  <!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

 

 
<script>
  $("#miModal").modal("show");
  $(document).on("click", ".edit", function () {
     var myBookId = $(this).data('id');
     var nombres = $(this).data('name');
     var paterno = $(this).data('paterno');
     var materno = $(this).data('materno');
     var maestria = $(this).data('maestria');
     var doctorado = $(this).data('doctorado');
     var especialidad = $(this).data('especialidad');
     var genero = $(this).data('genero');
     var email = $(this).data('email');
     var dni = $(this).data('dni');
     var imagen = $(this).data('imagen');
     var celular = $(this).data('celular');
     var img = $(this).data('img');

     $("#addBookDialog").modal("show");
     $(".modal-body #bookId").val( myBookId );
     $("#maestria").html( maestria );
     $("#doctorado").html( doctorado );
     $("#especialidad").html( especialidad );
     $("#email").html( email );
     $("#paterno").html( paterno );
     $("#materno").html( materno );
     $("#nombres").html( nombres );
     $("#dni").html( dni );
     $("#celular").html( celular );
     $("#imagen").html( imagen );
     //$("#imagenmuestra").attr("src",imagen);
     if(img)
        $("#imagenmuestra").attr("src",imagen); 
     else
        $("#imagenmuestra").attr("src","{{asset('web/images/user.jpg')}}");
     
     //$(".modal-body #genero").val( genero );

     if(genero=='masculino')
      $("#genero").html('TRABAJADOR SOCIAL');
    else
      $("#genero").html('TRABAJADORA SOCIAL');

     // As pointed out in comments, 
     // it is unnecessary to have to manually call the modal.
     // $('#addBookDialog').modal('show');
});
</script>

    <!-- Javascript end
    <?php $clientes = App\Models\Clientes::orderBy('id', 'desc')->get();?>
    @foreach($clientes as $key=>$cliente)
    <div class="modal fade" id="exampleModalCenter{{ $key + 1 }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" data>
          <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle{{ $key + 1 }}">Condiciones</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <center><b>{!! $cliente->nombres !!}</b></center>
                <center><b>{!! $cliente->paterno !!} {!! $cliente->materno !!}</b></center>

                <center>ID No: {{$cliente->ctsp}} </center><br>
                
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal" style="color:#fff;background-color: #575AC5">Cerrar</button>
              </div>
            </div>
          </div>
    </div>
    @endforeach
    

    <div class="container-all" id="modal">
        
        <div class="popup">
            <div class="img"></div>
            <div class="container-text">
                <h1>Lorem ipsum dolor sit amet, consectetur.</h1>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quisquam esse illum exercitationem perferendis accusamus, possimus sed molestiae accusantium necessitatibus neque sit aspernatur</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Possimus facere, sequi. Beatae recusandae, officiis sapiente amet quod vero est vel.</p>
            </div>
            
            <a href="#" class="btn-close-popup">X</a>
        </div>
        
    </div>-->
<!-- Enlace para abrir el modal -->
<div class="modal fade" id="addBookDialog" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" data>
    <div class="modal-dialog modal-dialog-centered modal-lg"   role="document">
      <!--<div class="modal-dialog modal-lg">-->
        <div class="modal-content" style="width:898px; height: 720px;">
            <div class="modal-body" style="padding-left: 15px">
              <div>
                <button class="btn-close-popup" data-dismiss="modal">X</button>
            </div>
            <div class="row imagen" style="-webkit-background-size: cover;-moz-background-size: cover;-o-background-size: cover;background-size: cover;background-repeat: no-repeat;height: 691px;width: 886px; padding-left: 70px">
                <div class="col-sm-6" >
                  <div class="form-group" >
                    </br></br>
                    <p class="espacio">
                        
                        <img class="imagencolegiado" src="" id="imagenmuestra"/>
                      
                    </p>
                    <p class="espacionombres"align="center">
                      <label class="letranombres" id="nombres" style="color: #000000;  font-weight: bold;"></label><br>
                      <label class="letraapellidos" style=" color: #000000;  text-align: center; font-weight: bold;" id="paterno"></label>&nbsp<label class="letraapellidos" id="materno" style="color: #000000;  font-weight: bold;"></label><br>
                      <p align="center" class="letratraconcod">
                        <label class="letratrabajador" id="genero" style=" color: #000000; "></label><br>
                        <input class="letracodigo" type="text" name="bookId" id="bookId" style=" border: 0; color: #1463E6; background-color:transparent;  height: 1px;  text-align: center; font-weight: bold;" readonly>
                      </p>
                    </p>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                      <div class="info">
                          <p class="hyphenation espaciodni" id="dni"></p>
                          <p class="hyphenation espacioemail" id="email"></p>
                          <p class="hyphenation espaciocelular" id="celular"></p>
                          <p class="hyphenation espaciomaestria" id="maestria"></p>
                          <p class="hyphenation espaciodoctorado" id="doctorado"></p>
                          <p class="hyphenation espacioespecialidad" id="especialidad"></p>
                      </div>
                  </div>
                </div>           
            </div>
        </div>
    </div>
</div>


</body>
<!-- Copied from http://themetechmount.com/html/boldman/index.html by Cyotek WebCopy 1.7.0.600, jueves, 12 de diciembre de 2019, 10:30:01 p.m -->
</html>
