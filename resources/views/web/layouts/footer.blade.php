<!-- WhatsHelp.io widget -->
<!-- GetButton.io widget -->
<script type="text/javascript">
    (function () {
        var options = {
            whatsapp: "+51985422690", // WhatsApp number
            call_to_action: "Escríbenos", // Call to action
            position: "left", // Position may be 'right' or 'left'
        };
        var proto = document.location.protocol, host = "getbutton.io", url = proto + "//static." + host;
        var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = url + '/widget-send-button/js/init.js';
        s.onload = function () { WhWidgetSendButton.init(host, proto, options); };
        var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x);
    })();
</script>
<!-- /GetButton.io widget -->
<!-- /WhatsHelp.io widget -->
      <section class="ttm-row desc-only-section ttm-bgcolor-skincolor text-center clearfix">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <h6 style="color:#fff">PART. REG. N 11313302 - SUNARP</a></h6>
                    </div>
                </div>
            </div>
        </section>

<footer class="footer widget-footer clearfix" style="background-color: #182434">

         <div class="second-footer ttm-textcolor-white">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-3 widget-area">
                        <div class="widget clearfix">
                            <div class="footer-logo" style="padding-top: 50px">
                                <img id="footer-logo-img" class="img-center" src="{{asset('web/images/logofooter.png')}}" alt="" style="width: 100%">
                            </div>
                        </div>

                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-3 widget-area">

                            <div class="widget widget_text clearfix">
                            <h3 class="widget-title">Horario de Atención</h3>
                            <div class="textwidget widget-text">
                                <div class="ttm-pricelistbox-wrapper ">
                                    <div class="ttm-timelist-block-wrapper">
                                        <ul class="ttm-timelist-block">
                                            <li>LUN. A VIE. <span class="service-time">09 AM - 01 PM</span></li>
                                            <li>LUN. A VIE. <span class="service-time">04 PM - 07 PM</span></li>
                                            <li>SAB <span class="service-time">09 AM - 01 PM</span></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-3 widget-area">
                        <div class="widget widget_nav_menu clearfix">
                           <h3 class="widget-title">Menú Rápido</h3>
                            <ul id="menu-footer-services">
                                <li><a href="{{route('/')}}">Inicio</a></li>
                                <li><a href="{{route('tramites')}}">Tr&aacute;mites</a></li>
                                <li><a href="{{route('mision')}}">Institución</a></li>
                                <li><a href="{{route('contacto')}}">Cont&aacute;ctenos</a></li>
                                <li><a href="{{route('eventos')}}">Eventos</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-3 widget-area">
                        <div class="widget widget_text clearfix">
                           <h3 class="widget-title">Cont&aacute;ctenos</h3>
                           <div class="textwidget widget-text">
                                <h3 class="ttm-textcolor-skincolor">(+51) 985422690</h3>
                                <p>ctspregion.lac@gmail.com</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="bottom-footer-text ttm-textcolor-white">
            <div class="container">
                <div class="row copyright">
                    <div class="col-md-8 ttm-footer2-left">
                        <span>Copyright © 2019&nbsp;CTSP REG II</a>. Todos los derechos reservados.</span>
                    </div>
                    <div class="col-md-4 ttm-footer2-right">
                       <div class="social-icons">
                            <ul class="list-inline">
                                <li><a href="https://www.facebook.com/pg/CtspRegionll" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                <li><a href="https://twitter.com/CtspRegionII" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                <li><a href="https://www.instagram.com/ctsp.regionll/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>

                                  <li><a href="https://api.whatsapp.com/send?phone=51985422690" target="_blank"><i class="fa fa-whatsapp" aria-hidden="true"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
