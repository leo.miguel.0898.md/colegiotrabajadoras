<div id="left-sidebar" class="sidebar">
    <div class="sidebar-scroll">
        <div class="user-account">
            <img src="{{asset('admin/images/user.jpg')}}" class="rounded-circle user-photo"
                 alt="User Profile Picture">
            <div class="dropdown">
                <span>Bienvenido,</span>
                <a href="javascript:void(0);" class="dropdown-toggle user-name"
                   data-toggle="dropdown"><strong>admin</strong></a>
                <ul class="dropdown-menu dropdown-menu-right account">
                    <li><a href="#"><i class="icon-user"></i>Perfil</a></li>
                    <li><a href="{{ route('logout') }}"><i class="icon-power"></i>Salir</a></li>
                </ul>
            </div>
            <hr>
        </div>
        <!-- Nav tabs -->
        <ul class="nav nav-tabs">
            <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#menu">Menu</a></li>
            <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#info"><i class="icon-question"></i></a>
            </li>

        </ul>

        <!-- Tab panes -->
        <div class="tab-content p-l-0 p-r-0">
            <div class="tab-pane active" id="menu">
                <nav id="left-sidebar-nav" class="sidebar-nav">
                    @yield('menu_items')

                </nav>

            </div>
            <div class="tab-pane p-l-15 p-r-15" id="info">
                <ul class="right_chat list-unstyled">
                    <li class="menu-heading" style="font-weight: 600; padding-bottom: 10px;">Soporte Técnico</li>
                    <li class="online"><a href="javascript:void(0);"> </a>
                        <div class="media"><a href="javascript:void(0);"> <img class="media-object "
                                                                               src="https://mimercado.delivery/admin/pyrus/images/ICON01.png"
                                                                               alt=""> </a>
                            <div class="media-body"><a href="javascript:void(0);"> <span
                                            class="name">Pyrus Studio</span> <span class="message"></span></a><a
                                        href="https://www.pyrusstudio.com/" target="_blank">www.pyrusstudio.com</a>
                            </div>
                        </div>
                    </li>
                    <li class="online" style="padding-bottom: 20px;">
                        <div class="body" style="padding-left: 50px;"> Escribenos: <br> info@pyrusstudio.com <br>
                            pyrusstudio@hotmail.com <br> pyrusstudio@outlook.com <br></div>
                    </li>
                    <li class="online" style="padding-bottom: 20px;">
                        <div class="body" style="padding-left: 50px;"> Llamanos: <br> 968637864 <br></div>
                    </li>
                    <li class="online"><a href="javascript:void(0);"> </a>
                        <div class="media"><a href="javascript:void(0);"> <img class="media-object "
                                                                               src="https://mimercado.delivery/admin/pyrus/images/ICON02.png"
                                                                               alt=""> </a>
                            <div class="media-body"><a href="javascript:void(0);"> <span class="name">Pyrus HD</span>
                                    <span class="message"></span></a><a href="https://pyrushd.com/" target="_blank">www.pyrushd.com</a>
                            </div>
                        </div>
                    </li>
                    <li class="online" style="padding-bottom: 20px;">
                        <div class="body" style="padding-left: 50px;"> Escribenos: <br> soporte@pyrushd.com <br>
                            pyrushd@hotmail.com <br></div>
                    </li>
                    <li class="online" style="padding-bottom: 20px;">
                        <div class="body" style="padding-left: 50px;"> Llamanos: <br> 995374822 <br></div>
                    </li>
                </ul>
            </div>

        </div>
    </div>
</div>
