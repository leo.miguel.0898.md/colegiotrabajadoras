<ul id="main-menu" class="metismenu">
    <li>
        <a href="#" class="has-arrow"><i class="icon-home"></i> <span>Gestionar</span></a>
        <ul>
            <li><a href="{{route('sliders.index')}}">Sliders</a></li>
            <li><a href="{{route('eventos.index')}}">Eventos</a></li>
            <li><a href="{{route('clientes.index')}}">Miembros</a></li>
            <li><a href="{{route('comunicados.index')}}">Comunicados</a></li>
            <li><a href="{{route('tramites.index')}}">Tramites</a></li>
        </ul>
    </li>
</ul>
