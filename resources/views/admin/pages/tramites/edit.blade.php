@extends('home')
@section('styles')
    <link rel="stylesheet" href="{{ asset('admin/vendor/dropify/css/dropify.min.css')}}"/>
    <link rel="stylesheet" href="{{ asset('admin/vendor/summernote/dist/summernote.css')}}"/>
@endsection
@section('titulo')
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-8 col-sm-12">
                <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i
                                class="fa fa-arrow-left"></i></a> Home</h2>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('home')}}"><i class="icon-home"></i></a></li>
                    <li class="breadcrumb-item">Home</li>
                    <li class="breadcrumb-item active">Editar</li>
                </ul>
            </div>

        </div>
    </div>

@endsection

@section('content')
 <div class="row clearfix">
        <div class="col-md-12">
            <div class="card">
                <div class="header">
                    <h2>Editar Tramites</h2>
                </div>
                <hr>
                <div class="body">
                    @include('error')
<form   action="{{route('tramites.update', $tramites->id)}}" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        {{ method_field('PUT')}}
                        <div class="form-group">
                            <div class="row clearfix">
                                <div class="col-md-6">
                                    <label for="">Titulo</label>
                                    <input type="text" class="form-control" name="nombre" value="{{$tramites->nombre}}" placeholder="titulo" require>
                                </div>
                                <div class="col-md-6">
                                <label for="">Descripcion</label>
                                    <input type="text" class="form-control" name="descripcion" value="{{$tramites->descripcion}}" placeholder="Descripcion" require>
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="row clearfix">
                                <div class="col-md-12">
                                <label for="">Imagen : (resolucion 1125 x 1599)</label>
                                <br>
                                    <img  src="{{ asset('admin/images/tramites/'.$tramites->img) }}" width="100">
                                    <input type="file" name="image" class="form-control">

                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row clearfix">
                                <div class="col-md-12">
                                <label for="">Imagen Portada : (resolucion 740 x 556)</label>
                                <br>
                                    <img  src="{{ asset('admin/images/tramites/'.$tramites->img_portada) }}" width="100">
                                    <input type="file" name="imagenew" class="form-control">

                                </div>
                            </div>
                        </div>
                                <button type="submit" class="btn btn-verde" >Guardar</button>
                                <a href="{{ route('tramites.index') }}" class="btn btn-verde">Cancelar</button></a>


                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{asset('admin/vendor/dropify/js/dropify.min.js')}}"></script>
    <script src="{{asset('admin/js/pages/forms/dropify.js')}}"></script>
    <script src="{{asset('admin/vendor/summernote/dist/summernote.js')}}"></script>
@endsection
