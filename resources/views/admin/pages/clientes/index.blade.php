@extends('home')
@section('styles')

    <link href="{{asset('admin/dataTables.bootstrap4.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('admin/responsive.bootstrap4.css')}}" rel="stylesheet" type="text/css">

@endsection
@section('titulo')
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-8 col-sm-12">
                <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i
                                class="fa fa-arrow-left"></i></a> Home</h2>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('home')}}"><i class="icon-home"></i></a></li>
                    <li class="breadcrumb-item">Home</li>
                    <li class="breadcrumb-item active">Miembros</li>
                </ul>
            </div>

        </div>
    </div>
@endsection
@section('content')
<div class="row clearfix">
    <div class="col-lg-12">
        <div class="card">
            <div class="header">
                @include('alerts')
                <h2 style="width: 70%;float: left;">Listado de Miembros </h2>
                <div ><a href="{{route('clientes.create')}}" class="btn btn-verde" style="float: right;">
                <span> Registrar</span></a></div>
                <div ><a href="{{route('excel.index')}}" class="btn btn-verde" style="float: right; background-color: #dc3545; border-color: #dc3545">
                <span> Exportar</span></a></div>
                <br>
                <br>
                <br>
                <br>
                <form method="post" action="{{route('importarExcel')}}" enctype="multipart/form-data" style="text-align: center;">
                                                        {{csrf_field()}}
                                                        <input type="file" name="excel" required class="form-control">
                                                        <br><br>
                                                        <div align="center">
                                                        <input type="submit" class="btn btn-dark" value="Importar">
                                                        </div>
                                            </form>
            </div>
            <div class="body">
                <div class="table-responsive">
                    <table id="basic-datatable" class="table table-striped nowrap">
                        <thead>
                            <tr>
                                <th>N°</th>
                                <th>Nombre</th>
                                <th>Apellidos</th>
                                <th>ctsp</th>
                                <th>estado</th>
                                <th>opciones</th>

                            </tr>
                            <tbody>
                                @foreach ($clientes as $key => $cliente)
                                    <tr>
                                        <td>{{ $key+1 }}</td>
                                        <td>{{ $cliente->nombres }}</td>
                                        <td>{{ $cliente->paterno}}</td>
                                        <td>{{ $cliente->ctsp}}</td>
                                        <td>{{ $cliente->estado}}</td>
                                        <td class="text-nowrap">
                                        <form method="POST"  action="{{route('clientes.destroy',$cliente->id)}}" >
                                            <a href="{{route('clientes.edit',$cliente->id)}}" class="btn btn-info" title="Editar"><i class="fa fa-edit"></i></a>&nbsp;
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}

                                                <button type="submit" class="btn btn-sm btn-danger" onclick="return confirm('Estas seguro de eliminar el cliente')"> <i class="fa fa-trash-o"></i></button>

                                             </form>
                                        </td>
                                    </tr>
                                @endforeach

                            </tbody>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')

    <script src="{{asset('admin/vendor/dropify/js/dropify.min.js')}}"></script>
    <script src="{{asset('admin/js/pages/forms/dropify.js')}}"></script>

@endsection
