@extends('home')
@section('header-editar')
<script src="//cdn.ckeditor.com/4.11.4/full/ckeditor.js"></script>
@endsection
@section('styles')

    <link href="{{asset('admin/dataTables.bootstrap4.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('admin/responsive.bootstrap4.css')}}" rel="stylesheet" type="text/css">

@endsection
@section('titulo')
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-8 col-sm-12">
                <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i
                                class="fa fa-arrow-left"></i></a> Home</h2>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('home')}}"><i class="icon-home"></i></a></li>
                    <li class="breadcrumb-item">Home</li>
                    <li class="breadcrumb-item active">Crear</li>
                </ul>
            </div>

        </div>
    </div>
@endsection
@section('content')
    <div class="row clearfix">
        <div class="col-md-12">
            <div class="card">
                <div class="header">
                    <h2>Subir Miembros</h2>
                </div>
                <hr>
                <div class="body">
                   @include('error')
                  <form   method="POST" action="{{route('clientes.store')}}" enctype="multipart/form-data" >
                  {{ csrf_field() }}

                    <div class="form-group">
                        <div class="row clearfix">
                            <div class="col-md-6">
                                <label for="">Nombres</label>
                                <input type="text" class="form-control" name="nombres" placeholder="nombres" require>
                            </div>
                            <div class="col-md-6">
                            <label for="">Apellido Paterno</label>
                                <input type="text" class="form-control" name="paterno" placeholder="paterno" require>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row clearfix">
                            <div class="col-md-6">
                                <label for="">Apellido Materno</label>
                                <input type="text" class="form-control" name="materno" placeholder="materno" require>
                            </div>
                            <div class="col-md-6">
                            <label for="">Ctsp</label>
                                <input type="text" class="form-control" name="ctsp" placeholder="ctsp" require>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row clearfix">
                            <div class="col-md-6">
                                <label for="">Estado</label>
                                <select type="text" class="form-control" name="estado" placeholder="estado" require>
                                <option value="">Seleccione</option>
                                <option value="habilitado">Habilitado</option>
                                <option value="inhabilitado">Inhabilitado</option>
                                </select>
                            </div>
                            <div class="col-md-6">
                            <label for="">Maestria</label>
                                <input type="text" class="form-control" name="maestria" placeholder="maestria" require>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row clearfix">
                            <div class="col-md-6">
                                <label for="">Doctorado</label>
                                <input type="text" class="form-control" name="doctorado" placeholder="doctorado" require>
                            </div>
                            <div class="col-md-6">
                            <label for="">Especialidad</label>
                                <input type="text" class="form-control" name="especialidad" placeholder="especialidad" require>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row clearfix">
                            <div class="col-md-6">
                                <label for="">Email</label>
                                <input type="email" class="form-control" name="email" placeholder="email" require>
                            </div>
                            <div class="col-md-6">
                                <label for="">Genero</label>
                                <select type="text" class="form-control" name="genero" placeholder="Género" require>
                                <option value="">Seleccione</option>
                                <option value="masculino">Masculino</option>
                                <option value="femenino">Femenino</option>
                                </select>
                            </div>
                        </div>
                    </div>
                     <div class="form-group">
                        <div class="row clearfix">
                            <div class="col-md-6">
                                <label for="">DNI</label>
                                <input type="text" class="form-control" name="dni" placeholder="dni" require>
                            </div>
                            <div class="col-md-6">
                                <label for="">Celular</label>
                                <input type="text" class="form-control" name="celular" placeholder="celular" require>
                            </div>
                        </div>
                    </div>
                <div class="form-group">


                            <label for="image">Imagen<strong>(resolucion A x A)</strong></label>
                            <input type="file" name="image" class="form-control" value="{{ old('image') }}" >



                    </div>
                            <button type="submit" class="btn btn-verde" >Subir</button>
                            <a href="{{ route('clientes.index') }}" class="btn btn-verde">Cancelar</button></a>


                  </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')

    <script src="{{asset('admin/vendor/dropify/js/dropify.min.js')}}"></script>
    <script src="{{asset('admin/js/pages/forms/dropify.js')}}"></script>
    <script >
        CKEDITOR.replace('content');
    </script>

@endsection
