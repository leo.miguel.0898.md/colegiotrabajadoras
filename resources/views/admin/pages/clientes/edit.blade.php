@extends('home')
@section('styles')
    <link rel="stylesheet" href="{{ asset('admin/vendor/dropify/css/dropify.min.css')}}"/>
    <link rel="stylesheet" href="{{ asset('admin/vendor/summernote/dist/summernote.css')}}"/>
@endsection
@section('titulo')
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-8 col-sm-12">
                <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a> Home</h2>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('home')}}"><i class="icon-home"></i></a></li>
                    <li class="breadcrumb-item">Home</li>
                    <li class="breadcrumb-item active">Miembros</li>
                </ul>
            </div>
        </div>
    </div>
@endsection
@section('content')
    <div class="row clearfix">
        <div class="col-md-12">
            <div class="card">
                <div class="header">
                    <h2>Editar Miembro</h2>
                </div>
                <hr>
                <div class="body">
                   @include('error')
                <form   action="{{route('clientes.update', $clientes->id)}}" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        {{ method_field('PUT')}}
                        <div class="form-group">
                            <div class="row clearfix">
                                <div class="col-md-6">
                                    <label for="">Nombres</label>
                                    <input type="text" class="form-control" name="nombres" value="{{$clientes->nombres}}" placeholder="Nombres" require>
                                </div>
                                <div class="col-md-6">
                                <label for="">Apellido Paterno</label>
                                    <input type="text" class="form-control" name="paterno" value="{{$clientes->paterno}}" placeholder="Apellido Paterno" require>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row clearfix">
                                <div class="col-md-6">
                                    <label for="">Apellido Materno</label>
                                    <input type="text" class="form-control" name="materno" value="{{$clientes->materno}}" placeholder="Apellido Materno" require>
                                </div>
                                <div class="col-md-6">
                                <label for="">CTSP</label>
                                    <input type="text" class="form-control" name="ctsp" value="{{$clientes->ctsp}}" placeholder="CTSP" require>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row clearfix">
                                <div class="col-md-6">
                                    <label for="">Estado</label>
                                    <select type="text" class="form-control" name="estado" value="{{$clientes->estado}}" placeholder="Estado" require>
                                        <option value="{{$clientes->estado}}">{{$clientes->estado}}</option>
                                        <option value="habilitado">Habilitado</option>
                                        <option value="inhabilitado">Inhabilitado</option>
                                    </select>
                                </div>
                                <div class="col-md-6">
                                <label for="">Maestría</label>
                                    <input type="text" class="form-control" name="maestria" value="{{$clientes->maestria}}" placeholder="Maestría" require>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row clearfix">
                                <div class="col-md-6">
                                    <label for="">Doctorado</label>
                                    <input type="text" class="form-control" name="doctorado" value="{{$clientes->doctorado}}" placeholder="Doctorado" require>
                                </div>
                                <div class="col-md-6">
                                <label for="">Especialidad</label>
                                    <input type="text" class="form-control" name="especialidad" value="{{$clientes->especialidad}}" placeholder="Especialidad" require>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row clearfix">
                                <div class="col-md-6">
                                    <label for="">Email</label>
                                    <input type="text" class="form-control" name="email" value="{{$clientes->email}}" placeholder="Email" require>
                                </div>
                                <div class="col-md-6">
                                    <label for="">Genero</label>
                                    <select type="text" class="form-control" name="genero" value="{{$clientes->genero}}" placeholder="Genero" require>
                                        <option value="{{$clientes->genero}}">{{$clientes->genero}}</option>
                                        <option value="masculino">Masculino</option>
                                        <option value="femenino">Femenino</option>
                                        
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row clearfix">
                                <div class="col-md-6">
                                    <label for="">DNI</label>
                                    <input type="text" class="form-control" name="dni" value="{{$clientes->dni}}" placeholder="dni" require>
                                </div>
                                <div class="col-md-6">
                                    <label for="">Celular</label>
                                    <input type="text" class="form-control" name="celular" value="{{$clientes->celular}}" placeholder="celular" require>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row clearfix">
                                <div class="col-md-12">
                                <label for="">Imagen : (resolucion 1920 x 700)</label>
                                <br>
                                    <img  src="{{ asset('admin/images/clientes/'.$clientes->img) }}" width="100">
                                    <input type="file" name="image" class="form-control">

                                </div>
                            </div>
                        </div>
                                <button type="submit" class="btn btn-verde" >Guardar</button>
                                <a href="{{ route('clientes.index') }}" class="btn btn-verde">Cancelar</button></a>
</form>
       </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{asset('admin/vendor/dropify/js/dropify.min.js')}}"></script>
    <script src="{{asset('admin/js/pages/forms/dropify.js')}}"></script>
    <script src="{{asset('admin/vendor/summernote/dist/summernote.js')}}"></script>
@endsection
