@extends('home')
@section('header-editar')
<script src="//cdn.ckeditor.com/4.11.4/full/ckeditor.js"></script>
@endsection
@section('styles')

    <link href="{{asset('admin/dataTables.bootstrap4.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('admin/responsive.bootstrap4.css')}}" rel="stylesheet" type="text/css">

@endsection
@section('titulo')
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-8 col-sm-12">
                <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i
                                class="fa fa-arrow-left"></i></a> Home</h2>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('home')}}"><i class="icon-home"></i></a></li>
                    <li class="breadcrumb-item">Home</li>
                    <li class="breadcrumb-item active">Crear</li>
                </ul>
            </div>

        </div>
    </div>
@endsection
@section('content')
    <div class="row clearfix">
        <div class="col-md-12">
            <div class="card">
                <div class="header">
                    <h2>Subir Comunicado</h2>
                </div>
                <hr>
                <div class="body">
                    @include('error')
                  <form   method="POST" action="{{route('comunicados.store')}}" enctype="multipart/form-data" >
                  {{ csrf_field() }}

                    <div class="form-group">
                        <div class="row clearfix">
                            <div class="col-md-6">
                                <label for="">Titulo</label>
                                <input type="text" class="form-control" name="nombre" placeholder="Titulo" require>
                            </div>
                            <div class="col-md-6">
                            <label for="">Descripcion</label>
                                <input type="text" class="form-control" name="descripcion" placeholder="descripcion" require>
                            </div>
                        </div>
                    </div>
                   
                    <div class="form-group">


                            <label for="image">Imagen<strong>(resolucion 654 x 494)</strong></label>
                            <input type="file" name="image" class="form-control" value="{{ old('image') }}" required>



                    </div>
                            <button type="submit" class="btn btn-verde" >Subir</button>
                            <a href="{{ route('comunicados.index') }}" class="btn btn-verde">Cancelar</button></a>


                  </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')

    <script src="{{asset('admin/vendor/dropify/js/dropify.min.js')}}"></script>
    <script src="{{asset('admin/js/pages/forms/dropify.js')}}"></script>
    <script >
        CKEDITOR.replace('content');
    </script>

@endsection
