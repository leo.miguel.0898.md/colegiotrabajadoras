<?php

namespace App\Models;
use App\DatesTranslate;
use Illuminate\Database\Eloquent\Model;

class Tramites extends Model
{
	use DatesTranslate;
	protected $table = "tramites";

    protected $fillable = [
        'img',
        'img_portada',
        'nombre',
        'descripcion',
    ];
}
