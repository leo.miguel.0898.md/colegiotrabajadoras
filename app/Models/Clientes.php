<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Clientes extends Model
{
     protected $table = "clientes";

    protected $fillable = [
        'nombres',
        'paterno',
        'materno',
        'img',
        'ctsp',
        'estado',
        'maestria',
        'doctorado',
        'especialidad',
        'email',
        'genero',
        'dni',
    ];
}
