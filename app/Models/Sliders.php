<?php

namespace App\Models;

use App\DatesTranslate;
use App\Models\Sliders;
use Illuminate\Database\Eloquent\Model;

class Sliders extends Model
{
    use DatesTranslate;
    protected $table = "sliders";

    protected $fillable = [

        'img',

    ];
}
