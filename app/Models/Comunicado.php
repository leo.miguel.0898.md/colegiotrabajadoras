<?php

namespace App\Models;
use App\DatesTranslate;
use Illuminate\Database\Eloquent\Model;

class Comunicado extends Model
{
	use DatesTranslate;
	protected $table = "comunicados";

    protected $fillable = [
        'img',
        'nombre',
        'descripcion',
    ];
}
