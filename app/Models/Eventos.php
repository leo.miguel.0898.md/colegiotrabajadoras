<?php

namespace App\Models;

use App\DatesTranslate;
use Illuminate\Database\Eloquent\Model;

class Eventos extends Model
{

    use DatesTranslate;
    protected $table = "eventos";

    protected $fillable = [
        'img',
        'nombre',
        'descripcion',
        'facebook',
    ];

}
