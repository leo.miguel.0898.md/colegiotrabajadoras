<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;

class EmailController extends Controller
{
    public function store(Request $request)
    {

        Mail::send('email.contact', $request->all(), function ($msj) {
            $msj->subject('correo de contacto');
            $msj->to('informes@ctspregion2.com');

        });
        Mail::send('email.contact', $request->all(), function ($msj) {
            $msj->subject('correo de contacto');
            $msj->to('ctspregion.lac@gmail.com');

        });

        return back()->with('flash', 'Su correo fue enviado correctamente');
    }
}
