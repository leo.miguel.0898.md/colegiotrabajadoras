<?php

namespace App\Http\Controllers;

use App\Models\Clientes;
use Maatwebsite\Excel\Facades\Excel;

class ExcelController extends Controller
{
    public function index()
    {
        Excel::create('Miembros CTSP', function ($excel) {

            $excel->sheet('Clientes', function ($sheet) {

                $clientes = Clientes::select('nombres', 'paterno', 'materno', 'ctsp', 'estado', 'maestria', 'doctorado', 'especialidad', 'email', 'genero', 'dni')->get();

                $sheet->fromArray($clientes);

            });
        })->export('xls');
    }
}
