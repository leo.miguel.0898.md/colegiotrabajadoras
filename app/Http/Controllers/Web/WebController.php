<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Clientes;
use App\Models\Eventos;
use App\Models\Sliders;
use App\Models\Tramites;
use App\Models\Comunicado;
use Carbon\Carbon;
use Illuminate\Http\Request;

class WebController extends Controller
{
    public function index()
    {
        $sliders = Sliders::orderBy('id', 'desc')->get();
        $date    = Carbon::now();
        $eventos = Eventos::orderBy('id', 'desc')->get();
        return view('web.pages.index')->with(compact('eventos'))->with(compact('sliders'));
    }
    public function institucional()
    {
        return view('web.pages.institucional');
    }
    public function historia()
    {
        return view('web.pages.historia');
    }
    public function mision()
    {
        return view('web.pages.mision');
    }
    public function consejos()
    {
        return view('web.pages.consejos');
    }
    public function requisitos()
    {
        return view('web.pages.requisitos');
    }
    public function contacto()
    {
        return view('web.pages.contacto');
    }
    public function eventos()
    {
        $eventos = Eventos::orderBy('id', 'desc')->get();
        return view('web.pages.eventos')->with(compact('eventos'));
    }
    public function detalleevento($id)
    {
        $evento  = Eventos::limit(5)->orderBy('id', 'desc')->get();
        $eventos = Eventos::findOrFail($id);
        return view('web.pages.detalleevento')->with(compact('eventos'))->with(compact('evento'));
    }
    public function tramites()
    {
        $tramites=Tramites::orderBy('id', 'desc')->get();
        return view('web.pages.tramites')->with(compact('tramites'));
    }
    public function detalletramites($id)
    {
        $tramite  = Tramites::limit(5)->orderBy('id', 'desc')->get();
        $tramites = Tramites::findOrFail($id);
        return view('web.pages.detalletramite')->with(compact('tramites'))->with(compact('tramite'));
    }
    public function paginas()
    {
        return view('web.pages.paginas');
    }
    public function comunicados()
    {
         $comunicados=Comunicado::orderBy('id', 'desc')->get();
        return view('web.pages.comunicados')->with(compact('comunicados'));
    }
    public function detallecomunicado($id)
    {
        $comunicado  = Comunicado::limit(5)->orderBy('id', 'desc')->get();
        $comunicados = Comunicado::findOrFail($id);
        return view('web.pages.detallecomunicado')->with(compact('comunicados'))->with(compact('comunicado'));
    }
    public function buscador(Request $request)
    {
/*
$nombres = Clientes::where("nombres", "like", $request->texto . "%")->take(10)->get();
return view('web.pages.buscador')->with(compact("nombres"));*/
        $nombres = $request->get('nombres');
        $paterno = $request->get('paterno');
        $materno = $request->get('materno');
        $ctsp    = $request->get('ctsp');

        $clientes = Clientes::orderBy('id', 'desc')
            ->where('nombres', 'LIKE', "%$nombres%")
            ->where('paterno', 'LIKE', "%$paterno%")
            ->where('materno', 'LIKE', "%$materno%")
            ->where('ctsp', 'LIKE', "%$ctsp%")
            ->get();
        return view('web.pages.buscador')->with(compact('clientes'));
    }
}
