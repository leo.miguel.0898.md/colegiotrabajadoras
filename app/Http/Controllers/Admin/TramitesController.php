<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Tramites;
use App\Http\Requests\TramitesRequest;
use DB;

class TramitesController extends Controller
{
    public function index()
    {
        $tramites = Tramites::orderBy('id', 'desc')->get();
        return view('admin.pages.tramites.index')->with(compact('tramites'));
    }

    public function create()
    {
        return view('admin.pages.tramites.create');
    }
    public function store(TramitesRequest $request)
    {

        try {
            $tramites              = new Tramites();
            $tramites->nombre      = $request->nombre;
            $tramites->descripcion = $request->descripcion;
            if ($request->file('image')) {
                $file = $request->file('image');
                $name = 'tramites_' . time() . '.' . $file->getClientOriginalExtension();
                $path = public_path() . '/admin/images/tramites/';
                $file->move($path, $name);
                $tramites->img = $name;
            }
            if ($request->file('imagenew')) {
                $file = $request->file('imagenew');
                $name = 'tramites_portada_' . time() . '.' . $file->getClientOriginalExtension();
                $path = public_path() . '/admin/images/tramites/';
                $file->move($path, $name);
                $tramites->img_portada = $name;
            }
            //$slider->name = $request->name;

            $tramites->save();
            DB::commit();
            return redirect()->route('tramites.index')->with('flash', 'El Tramite se ha creado correctamente');
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back();
        }

    }

    public function edit($id)
    {
        $tramites = Tramites::findOrFail($id);
        return view('admin.pages.tramites.edit')->with(compact('tramites'));
    }

    public function update(TramitesRequest $request, $id)
    {
        try {
            DB::beginTransaction();
            $tramite              = Tramites::findOrFail($id);
            $tramite->nombre      = $request->nombre;
            $tramite->descripcion = $request->descripcion;

            $pathToYourFile = public_path() . '/admin/images/tramites/' . $tramite->img;

            if ($request->file('image')) {

                if (file_exists($pathToYourFile)) {
                    unlink($pathToYourFile);
                }

                $file = $request->file('image');
                $name = 'tramites_' . time() . '.' . $file->getClientOriginalExtension();
                $path = public_path() . '/admin/images/tramites/';
                $file->move($path, $name);

                $tramite->img = $name;
            }
            $pathToYourFile = public_path() . '/admin/images/tramites/' . $tramite->img_portada;
            if ($request->file('imagenew')) {

                if (file_exists($pathToYourFile)) {
                    unlink($pathToYourFile);
                }

                $file = $request->file('imagenew');
                $name = 'tramites_portada_' . time() . '.' . $file->getClientOriginalExtension();
                $path = public_path() . '/admin/images/tramites/';
                $file->move($path, $name);

                $tramite->img_portada = $name;
            }

            $tramite->save();

            DB::commit();
            return redirect()->route('tramites.index')->with('flash', 'El Tramite se ha actualizado correctamente');
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back();
        }
    }
    public function show($id)
    {
        //
    }
    public function destroy($id)
    {
        //
        try {
            DB::beginTransaction();
            $tramite         = Tramites::findOrFail($id);
            $pathToYourFile = public_path() . '/admin/images/tramites/' . $tramite->img;
            if (file_exists($pathToYourFile)) {
                unlink($pathToYourFile);
            }
            $tramite->delete();
            DB::commit();
            return redirect()->back()->with('flash', 'El tramite se ha eliminado correctamente');;
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back();
        }
    }
}
