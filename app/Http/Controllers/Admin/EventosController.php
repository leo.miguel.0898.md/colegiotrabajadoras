<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\EventosRequest;
use App\Models\Eventos;
use DB;

class EventosController extends Controller
{
    public function index()
    {
        $eventos = Eventos::orderBy('id', 'desc')->get();
        return view('admin.pages.eventos.index')->with(compact('eventos'));
    }

    public function create()
    {
        return view('admin.pages.eventos.create');
    }
    public function store(EventosRequest $request)
    {

        try {
            $eventos              = new Eventos();
            $eventos->nombre      = $request->nombre;
            $eventos->descripcion = $request->descripcion;
            $eventos->facebook    = $request->facebook;
            if ($request->file('image')) {
                $file = $request->file('image');
                $name = 'eventos_' . time() . '.' . $file->getClientOriginalExtension();
                $path = public_path() . '/admin/images/eventos/';
                $file->move($path, $name);
                $eventos->img = $name;
            }
            //$slider->name = $request->name;

            $eventos->save();
            DB::commit();
            return redirect()->route('eventos.index')->with('flash', 'El Evento se ha creado correctamente');
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back();
        }

    }

    public function edit($id)
    {
        $eventos = Eventos::findOrFail($id);
        return view('admin.pages.eventos.edit')->with(compact('eventos'));
    }

    public function update(EventosRequest $request, $id)
    {
        try {
            DB::beginTransaction();
            $evento              = Eventos::findOrFail($id);
            $evento->nombre      = $request->nombre;
            $evento->descripcion = $request->descripcion;
            $evento->facebook    = $request->facebook;

            $pathToYourFile = public_path() . '/admin/images/eventos/' . $evento->img;

            if ($request->file('image')) {

                if (file_exists($pathToYourFile)) {
                    unlink($pathToYourFile);
                }

                $file = $request->file('image');
                $name = 'eventos_' . time() . '.' . $file->getClientOriginalExtension();
                $path = public_path() . '/admin/images/eventos/';
                $file->move($path, $name);

                $evento->img = $name;
            }

            $evento->save();

            DB::commit();
            return redirect()->route('eventos.index')->with('flash', 'El Evento se ha actualizado correctamente');
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back();
        }
    }
    public function show($id)
    {
        //
    }
    public function destroy($id)
    {
        //
        try {
            DB::beginTransaction();
            $evento         = Eventos::findOrFail($id);
            $pathToYourFile = public_path() . '/admin/images/eventos/' . $evento->img;
            if (file_exists($pathToYourFile)) {
                unlink($pathToYourFile);
            }
            $evento->delete();
            DB::commit();
            return redirect()->back()->with('flash', 'El evento se ha eliminado correctamente');;
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back();
        }
    }
}
