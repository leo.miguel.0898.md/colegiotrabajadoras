<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Comunicado;
use App\Http\Requests\ComunicadosRequest;
use DB;

class ComunicadosController extends Controller
{
    public function index()
    {
        $comunicados = Comunicado::orderBy('id', 'desc')->get();
        return view('admin.pages.comunicados.index')->with(compact('comunicados'));
    }

    public function create()
    {
        return view('admin.pages.comunicados.create');
    }
    public function store(ComunicadosRequest $request)
    {

        try {
            $comunicados              = new Comunicado();
            $comunicados->nombre      = $request->nombre;
            $comunicados->descripcion = $request->descripcion;
            if ($request->file('image')) {
                $file = $request->file('image');
                $name = 'comunicados_' . time() . '.' . $file->getClientOriginalExtension();
                $path = public_path() . '/admin/images/comunicados/';
                $file->move($path, $name);
                $comunicados->img = $name;
            }
            //$slider->name = $request->name;

            $comunicados->save();
            DB::commit();
            return redirect()->route('comunicados.index')->with('flash', 'El Comunicado se ha creado correctamente');
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back();
        }

    }

    public function edit($id)
    {
        $comunicados = Comunicado::findOrFail($id);
        return view('admin.pages.comunicados.edit')->with(compact('comunicados'));
    }

    public function update(ComunicadosRequest $request, $id)
    {
        try {
            DB::beginTransaction();
            $comunicado              = Comunicado::findOrFail($id);
            $comunicado->nombre      = $request->nombre;
            $comunicado->descripcion = $request->descripcion;

            $pathToYourFile = public_path() . '/admin/images/comunicados/' . $comunicado->img;

            if ($request->file('image')) {

                if (file_exists($pathToYourFile)) {
                    unlink($pathToYourFile);
                }

                $file = $request->file('image');
                $name = 'comunicados_' . time() . '.' . $file->getClientOriginalExtension();
                $path = public_path() . '/admin/images/comunicados/';
                $file->move($path, $name);

                $comunicado->img = $name;
            }

            $comunicado->save();

            DB::commit();
            return redirect()->route('comunicados.index')->with('flash', 'El Comunicado se ha actualizado correctamente');
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back();
        }
    }
    public function show($id)
    {
        //
    }
    public function destroy($id)
    {
        //
        try {
            DB::beginTransaction();
            $comunicado         = Comunicado::findOrFail($id);
            $pathToYourFile = public_path() . '/admin/images/comunicados/' . $comunicado->img;
            if (file_exists($pathToYourFile)) {
                unlink($pathToYourFile);
            }
            $comunicado->delete();
            DB::commit();
            return redirect()->back()->with('flash', 'El comunicado se ha eliminado correctamente');;
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back();
        }
    }
}
