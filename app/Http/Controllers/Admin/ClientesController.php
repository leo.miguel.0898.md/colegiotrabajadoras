<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ClientesRequest;
use App\Models\Clientes;
use DB;
use Excel;
use Illuminate\Http\Request;

class ClientesController extends Controller
{
    public function importarExcel(Request $request)
    {
        $clientes = Clientes::all();
        // dd($analisis);
        \Excel::load($request->excel, function ($reader) {
            $excel = $reader->get();

            $reader->each(function ($row) {
                $clientes                   = new Clientes;
                $clientes->nombres          = $row->nombres;
                $clientes->paterno          = $row->paterno;
                $clientes->materno          = $row->materno;
                $clientes->ctsp             = $row->ctsp;
                $clientes->estado           = $row->estado;
                $clientes->maestria         = $row->maestria;
                $clientes->doctorado        = $row->doctorado;
                $clientes->email            = $row->email;
                $clientes->img              = $row->img;
                $clientes->especialidad     = $row->especialidad;
                $clientes->genero = $row->genero;
                $clientes->dni   = $row->dni;
                $clientes->save();
            });
        });

        return back()->with('flash', 'Los miembros se han cargado correctamente');
    }
    public function index()
    {
        $clientes = Clientes::orderBy('id', 'desc')->get();
        return view('admin.pages.clientes.index')->with(compact('clientes'));
    }

    public function create()
    {
        return view('admin.pages.clientes.create');
    }
    public function store(ClientesRequest $request)
    {

        
            $clientes                   = new Clientes();
            $clientes->nombres          = $request->nombres;
            $clientes->paterno          = $request->paterno;
            $clientes->materno          = $request->materno;
            $clientes->ctsp             = $request->ctsp;
            $clientes->estado           = $request->estado;
            $clientes->maestria         = $request->maestria;
            $clientes->doctorado        = $request->doctorado;
            $clientes->especialidad     = $request->especialidad;
            $clientes->email            = $request->email;
            $clientes->genero = $request->genero;
            $clientes->dni   = $request->dni;
            $clientes->celular   = $request->celular;
            if ($request->file('image')) {
                $file = $request->file('image');
                $name = 'clientes_' . time() . '.' . $file->getClientOriginalExtension();
                $path = public_path() . '/admin/images/clientes/';
                $file->move($path, $name);
                $clientes->img = $name;
            }
            $clientes->save();
            DB::commit();
            return redirect()->route('clientes.index')->with('flash', 'El Miembro se ha creado correctamente');
        

    }

    public function edit($id)
    {
        $clientes = Clientes::findOrFail($id);
        return view('admin.pages.clientes.edit')->with(compact('clientes'));
    }

    public function update(ClientesRequest $request, $id)
    {
        //
        DB::beginTransaction();
        $clientes                   = Clientes::findOrFail($id);
        $clientes->nombres          = $request->nombres;
        $clientes->paterno          = $request->paterno;
        $clientes->materno          = $request->materno;
        $clientes->ctsp             = $request->ctsp;
        $clientes->estado           = $request->estado;
        $clientes->maestria         = $request->maestria;
        $clientes->doctorado        = $request->doctorado;
        $clientes->especialidad     = $request->especialidad;
        $clientes->email            = $request->email;
        $clientes->genero = $request->genero;
        $clientes->dni   = $request->dni;
        $clientes->celular   = $request->celular;

        $pathToYourFile = public_path() . '/admin/images/clientes/' . $clientes->img;

        if ($request->file('image')) {

            $file = $request->file('image');
            $name = 'clientes_' . time() . '.' . $file->getClientOriginalExtension();
            $path = public_path() . '/admin/images/clientes/';
            $file->move($path, $name);

            $clientes->img = $name;
        }

        $clientes->save();

        DB::commit();
        return redirect()->route('clientes.index')->with('flash', 'El Miembro se ha actualizado correctamente');

    }
    public function show($id)
    {
        //
    }
    public function destroy($id)
    {
        //
        try {
            DB::beginTransaction();
            $cliente        = Clientes::findOrFail($id);
            $pathToYourFile = public_path() . '/admin/images/clientes/' . $cliente->img;

            $cliente->delete();
            DB::commit();
            return redirect()->back()->with('flash', 'El miembro se ha eliminado correctamente');
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back();
        }
    }
}
