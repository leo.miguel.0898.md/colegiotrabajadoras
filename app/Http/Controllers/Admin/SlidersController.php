<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Sliders;
use DB;
use Illuminate\Http\Request;

class SlidersController extends Controller
{
    public function index()
    {
        //
        $sliders = Sliders::orderBy('id', 'desc')->get();
        return view('admin.pages.sliders.index')->with(compact('sliders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.pages.sliders.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        try {
            DB::beginTransaction();

            $slider = new Sliders();

            //dd($sliders->titulo);
            if ($request->file('image')) {
                $file = $request->file('image');
                $name = 'slider_' . time() . '.' . $file->getClientOriginalExtension();
                $path = public_path() . '/admin/images/sliders/';
                $file->move($path, $name);
                $slider->img = $name;
            }

            //$slider->name = $request->name;

            $slider->save();
            DB::commit();
            return redirect()->route('sliders.index')->with('flash', 'El Slider se ha creado correctamente');
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $sliders = Sliders::findOrFail($id);
        return view('admin.pages.sliders.edit')->with(compact('sliders'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        try {
            DB::beginTransaction();
            $slider = Sliders::findOrFail($id);

            $pathToYourFile = public_path() . '/admin/images/sliders/' . $slider->img;

            if ($request->file('image')) {

                if (file_exists($pathToYourFile)) {
                    unlink($pathToYourFile);
                }

                $file = $request->file('image');
                $name = 'slider_' . time() . '.' . $file->getClientOriginalExtension();
                $path = public_path() . '/admin/images/sliders/';
                $file->move($path, $name);

                $slider->img = $name;
            }

            $slider->save();

            DB::commit();
            return redirect()->route('sliders.index')->with('flash', 'El Slider se ha actualizado correctamente');
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        try {
            DB::beginTransaction();
            $slider         = Sliders::findOrFail($id);
            $pathToYourFile = public_path() . '/admin/images/sliders/' . $slider->img;
            if (file_exists($pathToYourFile)) {
                unlink($pathToYourFile);
            }
            $slider->delete();
            DB::commit();
            return redirect()->back()->with('flash', 'El slider se ha eliminado correctamente');;
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back();
        }
    }
}
