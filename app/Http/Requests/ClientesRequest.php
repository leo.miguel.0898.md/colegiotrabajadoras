<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ClientesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombres'          => 'required | max:255',
            'paterno'          => 'required | max:255',
            'materno'          => 'required | max:255',
            'ctsp'             => 'required | numeric',
            'estado'           => 'required | max:255',
            'maestria'         => 'required | max:255',
            'doctorado'        => 'required | max:255',
            'especialidad'     => 'required | max:255',
            'email'            => 'required | max:255',
            'genero'           => 'required | max:255',
            'dni'   => 'numeric|required|min:100000|max:99999999',
        ];
    }
}
